//Simone Tugnetti

#include <iostream>//Includo la libreria iostream.
using namespace std;//Applico std ai comandi che ne hanno bisogno.
int main(){//Inizio del programma in main.
	char Periodo,Camera;//Dichiaro le variabili char.
	int CTot,PrezzoCamera,NGiorni;//dichiaro le variabili int.
	const float sconto=0.95;//Dichiaro la costante.
	cout<<"Inserire il periodo di stagione(A=alta;B=bassa)"<<endl;
	cin>>Periodo;
	cout<<"Inserire il tipo di camera(S=singola;D=doppia;T=tripla)"<<endl;
	cin>>Camera;
	cout<<"Inserire il numero di giorni(se piu' di 7 verra' aggiunto uno sconto)"<<endl;
	cin>>NGiorni;
	if(Periodo=='A')//Il comando if che esegue una scelta a seconda della scelta del periodo. 
	{
		switch(Camera){//il comando switch fa scegliere un valore a PrezzoCamera a seconda della scelta dell'utente.
			case 'S':
				PrezzoCamera=50;
				cout<<"Il prezzo della camera e'"<<PrezzoCamera<<"Euro"<<endl;
				break;
			case 'D':
				PrezzoCamera=90;
				cout<<"Il prezzo della camera e'"<<PrezzoCamera<<"Euro"<<endl;
				break;
			case 'T':
				PrezzoCamera=140;
				cout<<"Il prezzo della camera e'"<<PrezzoCamera<<"Euro"<<endl;
				break;
			default:
				cout<<"La camera non esiste"<<endl;
				break;
		}
	}
	else
	{
		switch(Camera){
			case 'S':
				PrezzoCamera=40;
				cout<<"Il prezzo della camera e'"<<PrezzoCamera<<"Euro"<<endl;
				break;
			case 'D':
				PrezzoCamera=75;
				cout<<"Il prezzo della camera e'"<<PrezzoCamera<<"Euro"<<endl;
				break;
			case 'T':
				PrezzoCamera=110;
				cout<<"Il prezzo della camera e'"<<PrezzoCamera<<"Euro"<<endl;
				break;
			default:
				cout<<"La camera non esiste"<<endl;
				break;
	}
	}
	
	CTot=PrezzoCamera*NGiorni;//Calcolo del costo totale.
		
	if(NGiorni>=7)//Comando if che applica lo sconto del 5% solo se i giorni di soggiorno sono pi� o uguali a 7.
	{
		CTot=CTot*sconto;//calcolo del costo totale applicando lo sconto.
	cout<<"Il costo totale e'"<<CTot<<"Euro"<<endl;
	}

	
	system("Pause");//Comando necessario affinch� la finestra di dialogo rimanga aperta.
}//fine programma.














