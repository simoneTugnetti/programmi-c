#include <iostream>
#include <string>
#define DIM 300
using namespace std;

// Simone Tugnetti

void aggiungiStudente(struct studente studenti[], int *numStudenti);
void aggiungiVoto(struct studente studenti[], int numStudenti);
void votiMigliori(struct studente studenti[], int numStudenti);
void votiPeggiori(struct studente studenti[], int numStudenti);
void ordinare(struct studente studenti[], int numStudenti);
void stampa(struct studente studenti[], int numStudenti);
int mediaVotiMax(struct studente studenti[],int numStudenti);
int mediaVotiMin(struct studente studenti[], int numStudenti);
float mediaVoti(float voti[], int numVoti);
void ordinaNome(struct studente studenti[], int numStudenti);
void ordinaCognome(struct studente studenti[], int numStudenti);
void ordinaVoti(struct studente studenti[], int numStudenti);

struct studente{
	int id,numVoti;
	string nome,cognome;
	float voti[DIM];
};

int main(){
	int scelta=1,numStudenti=0;
	struct studente studenti[DIM];
	while(scelta!=0){
		cout<<"\n\nCosa vuoi fare?";
		cout<<"\n1 - Inserisci uno studente";
		cout<<"\n2 - Inserisci il voto di uno studente";
		cout<<"\n3 - Ricercare lo studente con i voti migliori";
		cout<<"\n4 - Ricercare lo studente con i voti peggiori";
		cout<<"\n5 - Ordinare gli studenti";
		cout<<"\n6 - Stampa degli studenti e dei relativi voti";
		cout<<"\n0 - Esci\n";
		cin>>scelta;
		switch(scelta){
			case 1:
				if(numStudenti<DIM){
					aggiungiStudente(studenti,&numStudenti);
				}else{
					cout<<"\nNon e' piu' possibile inserire studenti";
				}
				break;
			case 2:
				if(numStudenti>0){
					aggiungiVoto(studenti,numStudenti);
				}else{
					cout<<"\nNon e' stato trovato alcuno studente";
				}
				break;
			case 3:
				if(numStudenti>0){
					votiMigliori(studenti,numStudenti);
				}else{
					cout<<"\nNon e' stato trovato alcuno studente";
				}
				break;
			case 4:
				if(numStudenti>0){
					votiPeggiori(studenti,numStudenti);
				}else{
					cout<<"\nNon e' stato trovato alcuno studente";
				}
				break;
			case 5:
				if(numStudenti>0){
					ordinare(studenti,numStudenti);
				}else{
					cout<<"\nNon e' stato trovato alcuno studente";
				}
				break;
			case 6:
				if(numStudenti>0){
					stampa(studenti,numStudenti);
				}else{
					cout<<"\nNon e' stato trovato alcuno studente";
				}
				break;
			case 0:
				cout<<"\nGrazie per aver utilizzato il nostro programa!";
				break;
			default:
				cout<<"\nScelta non valida, riprova!";
				break;
		}
	}
}

/**
Funzione che inserisce uno studente alla lista della classe
@PARAM array di studenti e numero di studenti effettivamente
salvati
@MODIFY array degli studenti aggiornato
*/
void aggiungiStudente(struct studente studenti[], int *numStudenti){
	int i=*numStudenti;
	cout<<"\nInserisci l'ID dello studente\n";
	cin>>studenti[i].id;
	cout<<"\nInserisci il nome dello studente\n";
	cin>>studenti[i].nome;
	cout<<"\nInserisci il cognome dello studente\n";
	cin>>studenti[i].cognome;
	studenti[i].numVoti=0;
	*numStudenti=*numStudenti+1;
}

/**
Funzione che inserisce un voto ad un determinato studente
@PARAM array di studenti e numero di studenti effettivamente
salvati
@MODIFY matrice dei voti aggiornata
*/
void aggiungiVoto(struct studente studenti[], int numStudenti){
	int i,j,id,trov=0;
	cout<<"\nInserire ID dello studente:\n";
	cin>>id;
	for(i=0;i<numStudenti && trov==0;i++){
		if(id==studenti[i].id){
			trov=1;
			j=i;
		}
	}
	if(trov==1){
		cout<<"\nInserisci il nuovo voto!\n";
		cin>>studenti[j].voti[studenti[j].numVoti];
		studenti[j].numVoti++;
	}else{
		cout<<"\nNon e' stato trovato alcuno studente";
	}
}

/**
Funzione che calcola lo studente con i voti migliori
@PARAM array di studenti e numero di studenti effettivamente
salvati
*/
void votiMigliori(struct studente studenti[], int numStudenti){
	int i=mediaVotiMax(studenti,numStudenti),j;
	cout<<"\nEcco lo studente con i voti migliori: ";
	cout<<"\n\nID: "<<studenti[i].id;
	cout<<"\nNome: "<<studenti[i].nome;
	cout<<"\nCognome: "<<studenti[i].cognome;
	cout<<"\nVoti:\n";
	for(j=0;j<studenti[i].numVoti;j++){
		cout<<studenti[i].voti[j]<<", ";
	}
	cout<<"\b\b.";
}

/**
Funzione che calcola la media pi� alta dei voti
@PARAM array degli studenti e numero di studenti effettivamente
salvati
@RETURN lo studente con la media pi� alta
*/
int mediaVotiMax(struct studente studenti[], int numStudenti){
	int i,j;
	float media,MAX=0;
	for(i=0;i<numStudenti;i++){
		media=mediaVoti(studenti[i].voti,studenti[i].numVoti);
		if(media>MAX){
			MAX=media;
			j=i;
		}
	}
	return j;
}

/**
Funzione che calcola lo studente con i voti migliori
@PARAM array di studenti e numero di studenti effettivamente
salvati
*/
void votiPeggiori(struct studente studenti[], int numStudenti){
	int i=mediaVotiMin(studenti,numStudenti),j;
	cout<<"\nEcco lo studente con i voti peggiori: ";
	cout<<"\n\nID: "<<studenti[i].id;
	cout<<"\nNome: "<<studenti[i].nome;
	cout<<"\nCognome: "<<studenti[i].cognome;
	cout<<"\nVoti:\n";
	for(j=0;j<studenti[i].numVoti;j++){
		cout<<studenti[i].voti[j]<<", ";
	}
	cout<<"\b\b.";
}

/**
Funzione che calcola la media pi� bassa dei voti
@PARAM array degli studenti e numero di studenti effettivamente
salvati
@RETURN lo studente con la media pi� bassa
*/
int mediaVotiMin(struct studente studenti[], int numStudenti){
	int i,j;
	float media,min=0;
	for(i=0;i<numStudenti;i++){
		media=mediaVoti(studenti[i].voti,studenti[i].numVoti);
		if(media<min || min==0){
			min=media;
			j=i;
		}
	}
	return j;
}

/**
Funzione che ordina gli studenti in base al cognome, al nome od
al voto
@PARAM array di studenti e numero di studenti effettivamente
salvati
*/
void ordinare(struct studente studenti[], int numStudenti){
	int scelta;
	cout<<"\nCome si intendono ordinare gli studenti?";
	cout<<"\n1 - Tramite nome";
	cout<<"\n2 - Tramite cognome";
	cout<<"\n3 - Tramite voti\n";
	cin>>scelta;
	switch(scelta){
		case 1:
			ordinaNome(studenti,numStudenti);
			break;
		case 2:
			ordinaCognome(studenti,numStudenti);
			break;
		case 3:
			ordinaVoti(studenti,numStudenti);
			break;
	}
}

/**
Funzione che ordina gli studenti per nome
@PARAM array di studenti e numero di studenti effettivamente
salvati
@MODIFY Vettore degli studenti ordinato per nome in modo
decrescente
*/
void ordinaNome(struct studente studenti[], int numStudenti){
	int i,j;
	struct studente tmp;
	for(i=0;i<numStudenti;i++){
		for(j=i+1;j<numStudenti;j++){
			if((studenti[i].nome.compare(studenti[j].nome))<0){
				tmp=studenti[i];
				studenti[i]=studenti[j];
				studenti[j]=tmp;
			}
		}
	}
}

/**
Funzione che ordina gli studenti per cognome
@PARAM array di studenti e numero di studenti effettivamente
salvati
@MODIFY Vettore degli studenti ordinato per cognome in modo
decrescente
*/
void ordinaCognome(struct studente studenti[], int numStudenti){
	int i,j;
	struct studente tmp;
	for(i=0;i<numStudenti;i++){
		for(j=i+1;j<numStudenti;j++){
			if((studenti[i].cognome.compare(studenti[j].cognome))<0){
				tmp=studenti[i];
				studenti[i]=studenti[j];
				studenti[j]=tmp;
			}
		}
	}
}

/**
Funzione che ordina gli studenti in base ai voti
@PARAM array di studenti e numero di studenti effettivamente
salvati
@MODIFY Vettore degli studenti ordinato in base ai voti in modo
decrescente
*/
void ordinaVoti(struct studente studenti[], int numStudenti){
	int i,j;
	struct studente tmp;
	float primaMedia,secondaMedia;
	for(i=0;i<numStudenti;i++){
		for(j=i+1;j<numStudenti;j++){
			primaMedia=mediaVoti(studenti[i].voti,studenti[i].numVoti);
			secondaMedia=mediaVoti(studenti[j].voti,studenti[j].numVoti);
			if(primaMedia<secondaMedia){
				tmp=studenti[i];
				studenti[i]=studenti[j];
				studenti[j]=tmp;
			}
		}
	}
}

/**
Funzione che calcola la media di uno specifico studente
@PARAM voti dello studente e numero dei voti posseduti
@RETURN media dei voti
*/
float mediaVoti(float voti[], int numVoti){
	int i;
	float media=0,somma=0;
	if(numVoti>0){
		for(i=0;i<numVoti;i++){
			somma+=voti[i];
		}
		media=(float)somma/numVoti;
	}
	return media;
}

/**
Funzione che stampa l'intero array di studenti
@PARAM array di studenti e numero di studenti effettivamente
salvati
*/
void stampa(struct studente studenti[], int numStudenti){
	int i,j;
	for(i=0;i<numStudenti;i++){
		cout<<"\n\nStudente numero "<<i+1;
		cout<<"\nID: "<<studenti[i].id;
		cout<<"\nNome: "<<studenti[i].nome;
		cout<<"\nCognome: "<<studenti[i].cognome;
		cout<<"\nVoti:\n";
		for(j=0;j<studenti[i].numVoti;j++){
			cout<<studenti[i].voti[j]<<", ";
		}
		cout<<"\b\b.";
	}
}

