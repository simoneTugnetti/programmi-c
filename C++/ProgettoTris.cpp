//Simone Tugnetti 3�AI
#include <iostream>
#include <string>
using namespace std;
const int lvet=2;//Indico la costante lvet.
int i,j,tris[lvet][lvet],k,w,punteggio[lvet];
string giocatore1,giocatore2;
char segno1,segno2;
void caricamento(){//Procedura del caricamento dei giocatori e dei rispettivi segni.
	cout<<"Inserisci il tuo nome:"<<endl;
	cin>>giocatore1;
	cout<<"Inserisci il tuo segno tra x oppure o:"<<endl;
	cin>>segno1;
	cout<<"E ora passiamo al prossimo giocatore!!!"<<endl;
	cout<<"Inserisci il tuo nome:"<<endl;
	cin>>giocatore2;
	cout<<"Inserisci il tuo segno tra x oppure o (se il giocatore precedente ha scelto uno dei due segni, inserisci il segno opposto):"<<endl;
	cin>>segno2;
}
void giocata(){//Procedura in cui inizia il gioco del tris.
	cout<<"Il gioco comincia!!!"<<endl;
	cout<<""<<endl;
	cout<<"      |         |        "<<endl;
	cout<<"      |         |        "<<endl;
	cout<<"------------------------"<<endl;
	cout<<"      |         |        "<<endl;
	cout<<"      |         |        "<<endl;
	cout<<"------------------------"<<endl;
	cout<<"      |         |        "<<endl;
	cout<<"      |         |        "<<endl;
	cout<<""<<endl;
	do{
		cout<<"Comincia "<<giocatore1<<"."<<endl;
		cout<<"Dove vuoi inserire "<<segno1<<"? (prima scrivi la riga e poi la colonna)"<<endl;
		do{
		cin>>i;
		cin>>j;
		}while(i>2||j>2);
		tris[i][j]=segno1;
		cout<<"E' il turno di "<<giocatore2<<"."<<endl;
		cout<<"Dove vuoi inserire "<<segno2<<"? (prima scrivi la riga e poi la colonna)"<<endl;
		do{
		cin>>k;
		cin>>w;
		}while(k>2||w>2);
		tris[k][w]=segno2;
	}while(tris[0][0]!=segno1&&tris[0][1]!=segno1&&tris[0][2]!=segno1||
			tris[0][0]!=segno1&&tris[1][0]!=segno1&&tris[2][0]!=segno1||
			tris[1][0]!=segno1&&tris[1][1]!=segno1&&tris[1][2]!=segno1||
			tris[2][0]!=segno1&&tris[2][1]!=segno1&&tris[2][2]!=segno1||
			tris[1][0]!=segno1&&tris[1][1]!=segno1&&tris[0][0]!=segno1||
			tris[0][1]!=segno1&&tris[1][1]!=segno1&&tris[2][1]!=segno1||
			tris[0][2]!=segno1&&tris[1][2]!=segno1&&tris[2][2]!=segno1||
			tris[0][0]!=segno1&&tris[1][1]!=segno1&&tris[2][2]!=segno1||
			tris[0][2]!=segno1&&tris[1][1]!=segno1&&tris[2][0]!=segno1||
			tris[0][0]!=segno2&&tris[0][1]!=segno2&&tris[0][2]!=segno2||
			tris[0][0]!=segno2&&tris[1][0]!=segno2&&tris[2][0]!=segno2||
			tris[1][0]!=segno2&&tris[1][1]!=segno2&&tris[1][2]!=segno2||
			tris[2][0]!=segno2&&tris[2][1]!=segno2&&tris[2][2]!=segno2||
			tris[1][0]!=segno2&&tris[1][1]!=segno2&&tris[0][0]!=segno2||
			tris[0][1]!=segno2&&tris[1][1]!=segno2&&tris[2][1]!=segno2||
			tris[0][2]!=segno2&&tris[1][2]!=segno2&&tris[2][2]!=segno2||
			tris[0][0]!=segno2&&tris[1][1]!=segno2&&tris[2][2]!=segno2||
			tris[0][2]!=segno2&&tris[1][1]!=segno2&&tris[2][0]!=segno2);//Vari controlli per sapere se effettivamente uno dei due giocatori � riuscito a fare tris.

			if(tris[0][0]==segno1&&tris[0][1]==segno1&&tris[0][2]==segno1||
			tris[0][0]==segno1&&tris[1][0]==segno1&&tris[2][0]==segno1||
			tris[1][0]==segno1&&tris[1][1]==segno1&&tris[1][2]==segno1||
			tris[2][0]==segno1&&tris[2][1]==segno1&&tris[2][2]==segno1||
			tris[1][0]==segno1&&tris[1][1]==segno1&&tris[0][0]==segno1||
			tris[0][1]==segno1&&tris[1][1]==segno1&&tris[2][1]==segno1||
			tris[0][2]==segno1&&tris[1][2]==segno1&&tris[2][2]==segno1||
			tris[0][0]==segno1&&tris[1][1]==segno1&&tris[2][2]==segno1||
			tris[0][2]==segno1&&tris[1][1]==segno1&&tris[2][0]==segno1){
				punteggio[0]=punteggio[0]+1;
			}
			else{
				punteggio[1]=punteggio[1]+1;
			}

			
}
void classifica(){//Procedura per scoprire quale giocatore ha vinto.
	if(punteggio[0]==10){
		cout<<"Bravo "<<giocatore1<<", hai vinto!!! COMPLIMENTONI!!!"<<endl;
	}
	else{
		cout<<"Bravo "<<giocatore2<<", hai vinto!!! COMPLIMENTONI!!!"<<endl;
	}
}
int main(){//procedura del men�.
	caricamento();
	do{
	giocata();
	}while(punteggio[0]<10||punteggio[1]<10);//Finch� almeno uno dei due giocatori non totalizzer� 10 punti, il gioco continuer�.
	classifica();
	system("Pause");//Programma in pausa.
}//Fine programma.
