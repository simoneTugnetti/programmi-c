#include <iostream>
using namespace std;
const int lvet=9;
int matrice[lvet][lvet],i,j,n,m;
void carica(){
	cout<<"Inizia la procedura di caricamento"<<endl;
	cout<<"Ora il programma scegliera' i numeri da inserire nella matrice"<<endl;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			matrice[i][j]=rand()%100+1;
		}
	}
}
void posizionemax(){
	int max;
	cout<<"Inizia la procedura di trovare il valore massimo in ogni riga"<<endl;
	max=matrice[0][0];
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			if(matrice[i][j]>max){
				max=matrice[i][j];
				cout<<"Il valore massimo e' nella riga "<<i<<" e nella colonna "<<j<<endl;
			}
		}
	}
}
void ripvalore(){
	int valore,contposiz;
	contposiz=0;
	cout<<"Inizia la procedura di controllo per trovare quante volte viene ripetuto un valore"<<endl;
	cout<<"Inserisci il valore"<<endl;
	cin>>valore;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			if(matrice[i][j]==valore){
				contposiz=contposiz+1;
			}
		}
	}
	cout<<"Il numero e' stato trovato "<<contposiz<<" volte"<<endl;
}
void azzvalinf5(){
	cout<<"Inizia la procedura per azzerare tutti i valori inferiori a 5"<<endl;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			if(matrice[i][j]<5){
				matrice[i][j]=0;
			}
		}
	}
}
void minogniriga(){
	int min;
	cout<<"Inizia la procedura per trovare il valore minimo in ogni riga"<<endl;
	for(i=0;i<n;i++){
		min=matrice[i][0];
		for(j=0;j<m;j++){
			if(matrice[i][j]<min){
				min=matrice[i][j];
			}
		}
		cout<<"Il valore minimo di questa riga e' "<<min<<endl;
	}
}
void valmaxcolonna(){
	int max;
	cout<<"Inizia la procedura per trovare il valore massimo in ogni colonna"<<endl;
	for(j=0;j<m;j++){
		max=matrice[0][j];
		for(i=0;i<n;i++){
			if(matrice[i][j]>max){
				max=matrice[i][j];
			}
		}
		cout<<"Il valore massimo di questa colonna e' "<<max<<endl;
	}
}
void numesist(){
	int num;
	bool trovato;
	cout<<"Inizia la procedura per trovare se un numero esiste oppure no"<<endl;
	cout<<"Inserisci un numero"<<endl;
	cin>>num;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			if(matrice[i][j]==num){
				trovato=true;
			}
		}
	}
	if(trovato==true){
		cout<<"Il numero esiste"<<endl;
	}
	else{
		cout<<"Il numero non esiste"<<endl;
	}
}
void sommarighe(){
	int somma;
	cout<<"Inizia la procedura per sommare le righe"<<endl;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			somma=matrice[i][j]+matrice[i][j+1];
		}
		cout<<"la somma dei numeri di questa riga e' "<<somma<<endl;
	}
}
void ordcrescmatrice(){
	int k,x,temp;
	cout<<"Inizia la procedura per ordinare la matrice in modo crescente"<<endl;
	for(k=0;k<10;k++){
		for(x=k+1;x<10;x++){
			for(i=0;i<n;i++){
				for(j=0;j<m;j++){
					if(matrice[i][j]<matrice[k][x]){
						temp=matrice[i][j];
						matrice[i][j]=matrice[k][x];
						matrice[k][x]=temp;
					}
				}
			}
		}
	}
}
int main(){
	n=10;
	m=10;
	carica();
	posizionemax();
	ripvalore();
	azzvalinf5();
	minogniriga();
	valmaxcolonna();
	numesist();
	sommarighe();
	ordcrescmatrice();
}