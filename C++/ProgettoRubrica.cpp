//Simone Tugnetti      4�AI
//Questo e' un progetto che emula una rubrica.
//Introduco le librerie.
#include <iostream>
#include <fstream>
#include <string>
using namespace std;//Richiamo il nome delle funzioni.
const int lvet=1000;//Assegno la costante al vettore.
//Creo una struttura denominata rubrica contenente tutti i dati inerenti alla persona.
struct rubrica{
	int tel;
	string nome;
	string cognome;
	string mail;
};
rubrica persona[lvet];//Utilizzo come nome del record "persona".
//Introduco le variabili globali.
int i,j,tmp1,telef,Npers,cont;
string tmp2,cognome3,nome1,mail1,cognome1,cognome2;
ofstream file1;
//Inizia la procedura di inserimento che mi permette di inserire i dati degli utenti vogliosi di essere aggiunti alla rubrica.
void Inserimento(){
	int conti;
	if(i>=999){//Se il numero di utenti arriva a 1000, il programma non permettera' piu' l'inserimento di nessun utente.
		cout<<"Hai raggiunto il numero massimo di utenti!!!"<<endl;
	}
	else{
	cont++;
	if (cont==1){//Se non e' stato inserito nessun utente, si iniziera' con l'inserimento da zero; altrimenti, si iniziera' dall'utimo utente inserito.
		do{
			cout<<"Inserisci il nome!!!"<<endl;
			cin>>persona[i].nome;
			cout<<"Inserisci il cognome!!!"<<endl;
			cin>>persona[i].cognome;
			cout<<"Inserisci il numero di telefono"<<endl;
			cin>>persona[i].tel;
			cout<<"Inserisci l'e-mail"<<endl;
			cin>>persona[i].mail;
			i++;
			cout<<"Hai finito???? (1 = Si; 2 = No)"<<endl;
			cin>>conti;
		}while(conti==2);
		Npers=i;
	}
	else{
		i=Npers+1;
		do{
			cout<<"Inserisci il nome!!!"<<endl;
			cin>>persona[i].nome;
			cout<<"Inserisci il cognome!!!"<<endl;
			cin>>persona[i].cognome;
			cout<<"Inserisci il numero di telefono"<<endl;
			cin>>persona[i].tel;
			cout<<"Inserisci l'e-mail"<<endl;
			cin>>persona[i].mail;
			i++;
			cout<<"Hai finito???? (1 = Si; 2 = No)"<<endl;
			cin>>conti;
		}while(conti==2);
		Npers=i;
		}
	}
}
//Inizia la procedura per modificare tutti i dati di un utente avendone solo il cognome.
void Modifica(){
	if(cont==0){
		cout<<"Inserisca prima dei dati nella rubrica per sbloccare questa funzione!!!"<<endl;
	}
	else{
	cout<<"Inserisci il cognome dell'utente da modificare!!!"<<endl;
	cin>>cognome1;
	for(i=0;i==Npers;i++){
		if(persona[i].cognome==cognome1){
			cout<<"Inserisci il nome!!!"<<endl;
			cin>>persona[i].nome;
			cout<<"Inserisci il cognome!!!"<<endl;
			cin>>persona[i].cognome;
			cout<<"Inserisci il numero di telefono"<<endl;
			cin>>persona[i].tel;
			cout<<"Inserisci l'e-mail"<<endl;
			cin>>persona[i].mail;
		}
	}
	}
}
//Inizia la procedura per eliminare tutti i dati di un utente avendone solo il cognome.
void Elimina(){
	if(cont==0){
		cout<<"Inserisca prima dei dati nella rubrica per sbloccare questa funzione!!!"<<endl;
	}
	else{
	cout<<"Inserisci il cognome dell'utente da eliminare!!!"<<endl;
	cin>>cognome2;
	for(i=0;i==Npers;i++){
		if(persona[i].cognome==cognome2){
			persona[i].nome="";
			persona[i].cognome="";
			persona[i].tel=0;
			persona[i].mail="";
		}
	}
	}
}
//Inizia la procedura per ordinare tutti i dati di un utente avendone solo il cognome.
void Ordinamento(){
	if(cont==0){
		cout<<"Inserisca prima dei dati nella rubrica per sbloccare questa funzione!!!"<<endl;
	}
	else{
	cout<<"La rubrica si ordinera' come da lei richiesto!!!"<<endl;
	for(i=0;i==Npers;i++){
		for(j=i+1;j==Npers;j++){
			if(persona[i].cognome>persona[j].cognome){
				tmp2=persona[i].cognome;
				persona[i].cognome=persona[j].cognome;
				persona[j].cognome=tmp2;
				tmp2=persona[i].nome;
				persona[i].nome=persona[j].nome;
				persona[j].nome=tmp2;
				tmp2=persona[i].mail;
				persona[i].mail=persona[j].mail;
				persona[j].mail=tmp2;
				tmp1=persona[i].tel;
				persona[i].tel=persona[j].tel;
				persona[j].tel=tmp1;
			}
		}
	}
	cout<<"I dati sono stati ordinati dalla A alla Z!!!!"<<endl;
	}
}
void Telefono(){
	cout<<"Inserisci il numero di telefono dell'utente da ricercare"<<endl;
	cin>>telef;
	for(i=0;i==Npers;i++){
		if(persona[i].tel==telef){
			cout<<persona[i].nome<<"             "<<persona[i].cognome<<"             "<<persona[i].tel<<"             "<<persona[i].mail<<endl;//Vengono visualizzati a schermo tutti i dati.
		}
	}
}
void Nome(){
	cout<<"Inserisci il nome dell'utente da ricercare"<<endl;
	cin>>nome1;
	for(i=0;i==Npers;i++){
		if(persona[i].nome==nome1){
			cout<<persona[i].nome<<"             "<<persona[i].cognome<<"             "<<persona[i].tel<<"             "<<persona[i].mail<<endl;
		}
	}
}
void Cognome(){
	cout<<"Inserisci il cognome dell'utente da ricercare"<<endl;
	cin>>cognome3;
	for(i=0;i==Npers;i++){
		if(persona[i].cognome==cognome3){
			cout<<persona[i].nome<<"             "<<persona[i].cognome<<"             "<<persona[i].tel<<"             "<<persona[i].mail<<endl;
		}
	}
}
void Mail(){
	cout<<"Inserisci la mail dell'utente da ricercare"<<endl;
	cin>>mail1;
	for(i=0;i==Npers;i++){
		if(persona[i].mail==mail1){
			cout<<persona[i].nome<<"             "<<persona[i].cognome<<"             "<<persona[i].tel<<"             "<<persona[i].mail<<endl;
		}
	}
}
//Inizia la procedura per ricercare tutti i dati di uno specifico utente utilizzando qualsiasi dato.
void Ricerca(){
	int scelta1;
	if(cont==0){
		cout<<"Inserisca prima dei dati nella rubrica per sbloccare questa funzione!!!"<<endl;
	}
	else{
	cout<<"Quale dato vuoi ricercare per trovare l'utente???"<<endl;
	cout<<"1 = Telefono"<<endl;
	cout<<"2 = Nome"<<endl;
	cout<<"3 = Cognome"<<endl;
	cout<<"4 = E-mail"<<endl;
	cin>>scelta1;
	switch (scelta1){//La scelta che si utilizzera' sara' corrispondente al dato da voler ricercare.
		case 1:
		Telefono();
		break;
	case 2:
		Nome();
		break;
	case 3:
		Cognome();
		break;
	case 4:
		Mail();
		break;
	}
	}
}
//Inizia la procedura per scrivere tutti i dati sul file di testo presente nella cartella del progetto.
void Scrivifile(){
	if(cont==0){
		cout<<"Doveva prima dei dati nella rubrica per sbloccare questa funzione!!!"<<endl;
		cout<<"Ma ormai e' TARDI!!!"<<endl;
	}
	else{
	cout<<"I dati verranno scritti sul file di testo!!!"<<endl;
	file1.open("file.txt", ios::app);//Apro il file utilizzando il tipo di aperturta app (scrivere su un file gia' presente).
	for(i=0;i==Npers;i++){
		file1<<persona[i].nome<<"             "<<persona[i].cognome<<"             "<<persona[i].tel<<"             "<<persona[i].mail<<endl;//Scrivo i dati sul file.
	}
	file1.close();//Chiudo il file.
	cout<<"I dati sono stati inseriti nel file!!!"<<endl;
	cout<<"Vada a vedere,prego."<<endl;
	}
}
//Inizia il menu' principale.
int main(){
	int scelta,risposta;
	i=0;
	cont=0;
	j=0;
	cout<<"Benvenuto in questa rubrica di ST S.r.l"<<endl;
	cout<<"Questa rubrica ha un massimo di 1000 persone"<<endl;
	do{
	cout<<"Fai la tua scelta:"<<endl;
	cout<<"1 = Inserimento dati"<<endl;
	cout<<"2 = Ricerca di dati"<<endl;
	cout<<"3 = Eliminazione di dati"<<endl;
	cout<<"4 = Ordinamento dei dati"<<endl;
	cout<<"5 = Modifica di dati"<<endl;
	cout<<"6 = Esci!!!"<<endl;
	cin>>scelta;
	switch (scelta){
	case 1:
		Inserimento();
		break;
	case 2:
		Ricerca();
		break;
	case 3:
		Elimina();
		break;
	case 4:
		Ordinamento();
		break;
	case 5:
		Modifica();
		break;
	case 6:
		exit(0);
		break;
	}
	cout<<"Hai finito????? 2(1 = Si; 2 = No)"<<endl;
	cin>>risposta;
	}while(risposta==2);//Se si vuole continuare con l'aggiunta di altri utenti, il ciclo continuera' all'infinito.
	Scrivifile();
	system("pause");//Il programma e' in pausa.
}//Fine del programma.

