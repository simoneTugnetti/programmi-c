#include <iostream>
using namespace std;
const int lvet=10;
int n,m,max[lvet],matrice[lvet][lvet],tmp,i,j;
void carica(){
	cout<<"Ora la matrice verra' caricata in modo casuale!!!"<<endl;
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			matrice[i][j]=rand()%100+1;
		}
	}
}
void valmaxrigaeord(){
	cout<<"Ora inizia la procedura per trovare i valori massimi per riga ordinandoli in modo crescente in un vettore!!!!"<<endl;
	for(i=0;i<n;i++){
		max[i]=matrice[i][0];
		for(j=0;j<m;j++){
			if(matrice[i][j]>max[i]){
				max[i]=matrice[i][j];
			}
		}
	}
	for(i=0;i<n;i++){
		for(j=0;j<m;j++){
			if(max[i]<max[j]){
				tmp=max[i];
				max[i]=max[j];
				max[j]=tmp;
			}
		}
	}
}
void ricdicot(){
	int valore,inizio,fine;
	bool trovato;
	cout<<"Ora inizia la procedura per trovare un valore nel vettore max utilizzando la ricerca dicotomica!!!"<<endl;
	cout<<"Inserisci il valore:"<<endl;
	cin>>valore;
	i=0;
	inizio=0;
	fine=n-1;
	trovato=false;
	while(trovato=false&&inizio<=fine){
		i=(inizio+fine)/2;
		if(valore==max[i]){
			trovato=true;
		}
		else{
			if(valore<max[i]){
				fine=i-1;
			}
			else{
				inizio=i+1;
			}
		}
	}
	if(trovato=true){
		cout<<"Il numero e' stato trovato!!! :D"<<endl;
	}
	else{
		cout<<"Il numero non e' stato trovato!!! :'("<<endl;
	}
}
void ricbubblesort(){
	cout<<"Ora inizia l'ordinamento in bubble sort del vettore max!!!"<<endl;
	for(j=0;j<1;j++){
		j=n;
		for(i=0;i<j;i++){
			if(max[i]>max[i+1]){
				tmp=max[i];
				max[i]=max[i+1];
				max[i+1]=tmp;
			}
			else{
				j=j-1;
			}
		}
	}
}
int main(){
	n=10;
	m=10;
	carica();
	valmaxrigaeord();
	ricdicot();
	ricbubblesort();
system("Pause");
}