#include <stdlib.h>
#include <stdio.h>

// Simone Tugnetti

int main(int argc,char *argv[]){
	int Num=0,C,Mol=100000;
		
	for(C=0;C<6;C++){
		Num=Num+(leggiCifra()*Mol);
		Mol=Mol/10;
	}
	
	printf("\nQuesto e' il valore decimale: %d",Num);

}

int leggiCifra(){
	char cifra;
	int cifraIntera;

	printf("\nInserisci la cifra da 0 a 9: ");
	cifra=getch();
	printf("%c",cifra);

	while(cifra<'0' || cifra>'9'){
		printf("\nValore non valido riprova: ");
		cifra=getch();
		printf("%c",cifra);
	}
	
	cifraIntera=cifra-'0';
	
	return cifraIntera;

}
