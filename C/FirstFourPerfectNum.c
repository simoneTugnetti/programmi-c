#include <stdlib.h>
#include <stdio.h>

// Simone Tugnetti

int main(int argc, char *argv[]){
	int Somma,Div,Num=1,C=0;
	
	printf("Ecco i primi 4 numeri perfetti --> ");

	while(C<4){
		Div=1;
		Somma=0;
		while(Div<=Num/2){
			if(Num%Div==0){
				Somma=Somma+Div;
			}
			Div++;
		}
		
		if(Somma==Num){
			printf("%d, ",Num);
			C++;
		}
		Num++;
	}
	
	printf("\b\b.");

}
