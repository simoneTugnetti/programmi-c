#include <stdio.h>
#include <stdlib.h>
#define DIM 4

// Simone Tugnetti

char InsColore();
void sceltaColori(char parola[], int dim);
void sceltaColoriSegreta(char colori[], int dim);
char minuToMaiu(char valore);
int gioco(char colori[]);
void controllo(char colori[], char tentColori[], char risultato[], int dim);
int vincPerd(char risultato[], int dim);
void stampaRisultato(char risultato[], int dim);

int main(int argv, char *argc[]){
	char colori[DIM];
	int tentativi,scelta,risultato;
	printf("Inserisci quanti tentativi vuoi effettuare --> ");
	scanf("%d",&tentativi);
	sceltaColoriSegreta(colori,DIM);
	printf("\nIl gioco ha inizio!");
	while(tentativi>0 && scelta!=0 && risultato==0){
		printf("\n\n1 --> Effettua un tentativo!");
		printf("\n0 --> Esci dal gioco!");
		printf("\n\nIntroduci la scelta --> ");
		scanf("%d",&scelta);
		switch(scelta){
			case 1:
				risultato=gioco(colori);
				tentativi--;
				if(risultato==0 && tentativi>0){
					printf("\nHai sbagliato, ma hai ancora %d tentativi!",tentativi);
				}else if(tentativi==0){
					printf("\nHai terminato i tentativi");
				}
				break;
			case 0:
				printf("\nNon ti abbattere, andra' meglio la prossima volta!");
				break;
			default:
				printf("\nOpzione non valida, riprova!");
				break;
		}
	}
	if(risultato==1){
		printf("\nComplimenti, hai vinto!!!");
	}else{
		printf("\nHai perso!\nMa non ti abbattere, andra' meglio la prossima volta!");
	}
	
}

/**
Chiede di inserire una lettera minuscola o Maiuscola, l'invio o
lo spazio
@RETURN carattere inserito
*/
char InsColore(){
	char lettera;
	lettera=getch();
	while((lettera!='a' && lettera!='b' && lettera!='g' && lettera!='m' && 
			lettera!='n' && lettera!='r' && lettera!='v') && (lettera!='A' && 
			lettera!='B' && lettera!='G' && lettera!='M' && lettera!='N' && lettera!='R' 
			&& lettera!='V')){
		lettera=getch();
	}
	lettera=minuToMaiu(lettera);
	return lettera;
}

/**
Sceglie i colori tra quelli disponibili in chiaro
@PARAM vettore dei colori da inserire e la dimensione del vettore
@MODIFY Il vettore dei colori risulta aggiornato
*/
void sceltaColori(char colori[], int dim){
	char Val;
	int i;
	printf("\nI colori possono essere --> A = Arancione - B = Blu - G = Giallo - M = Marrone - N = Nero - R = Rosso - V = Verde");
	printf("\nInserisci la combinazione di colori --> ");
	for(i=0;i<dim;i++){
		Val=InsColore();
		printf("%c",Val);
		colori[i]=Val;
	}
}

/**
Sceglie i colori tra quelli disponibili segretamente
@PARAM vettore dei colori da inserire e la dimensione del vettore
@MODIFY Il vettore dei colori risulta aggiornato
*/
void sceltaColoriSegreta(char colori[], int dim){
	char Val;
	int i;
	printf("\nI colori possono essere --> A = Arancione - B = Blu - G = Giallo - M = Marrone - N = Nero - R = Rosso - V = Verde");
	printf("\nInserisci la combinazione di colori segreta --> ");
	for(i=0;i<dim;i++){
		Val=InsColore();
		colori[i]=Val;
	}
}

/**
Funzione che converte una lettera minuscola in maiuscola
@PARAM lettera da convertire
@RETURN lettera convertita
*/
char minuToMaiu(char valore){
	if(valore>='a' && valore<='z'){
		valore=valore-('a'-'A');
	}
	return valore;
}

/**
Funzione che provvede alla gestione del gioco completo
@PARAM vettore dei colori da verificare
@RETURN variabile che identifica la vincita o la perdita
*/
int gioco(char colori[]){
	char tentColori[DIM],risultato[DIM];
	int epilogo;
	sceltaColori(tentColori,DIM);
	controllo(colori,tentColori,risultato,DIM);
	epilogo=vincPerd(risultato,DIM);
	stampaRisultato(risultato,DIM);
	return epilogo;
}

/**
Funzione che controlla quale colore � nella posizione esatta o
meno
@PARAM vettore dei colori iniziali, vettore dei colori da
confrontare, vettore dei risultati ottenuti e dimensione dei vettori
@MODIFY Il vettore dei risultati conterr� i risultati ottenuti
dai controlli
*/
void controllo(char colori[], char tentColori[], char risultato[], int dim){
	int i,j,k=0,trov=0;
	char colori2[DIM];
	for(i=0;i<dim;i++){
		colori2[i]=colori[i];
		risultato[i]='0';
	}
	for(i=0;i<dim;i++){
		if(tentColori[i]==colori2[i]){
			colori2[i]='0';
			risultato[k]='O';
			k++;
		}
	}
	for(i=0;i<dim;i++){
		trov=0;
		for(j=0;j<dim && trov==0;j++){
			if(tentColori[i]==colori2[j]){
				risultato[k]='o';
				k++;
				trov=1;
			}
		}
	}
}

/**
Funzione che stabilisce se il giocatore vince o perde in base ai
colori indovinati nella giusta posizione
@PARAM vettore dei risultati ottenuti e la dimensione del vettore
@RETURN variabile di verifica se la partita � stata vinta o persa
*/
int vincPerd(char risultato[], int dim){
	int i,trov=1;
	for(i=0;i<dim && trov==1;i++){
		if(risultato[i]!='O'){
			trov=0;
		}
	}
	return trov;
}

/**
Funzione che stampa i risultati ottenuti
@PARAM vettore dei risultati ottenuti e la dimensione del vettore
*/
void stampaRisultato(char risultato[], int dim){
	int i;
	for(i=0;i<dim;i++){
		if(risultato[i]=='0'){
			risultato[i]=0;
		}
	}
	printf("\nRisultato ottenuto --> ");
	for(i=0;i<dim;i++){
		printf("%c",risultato[i]);
	}
}

