#include <stdio.h>
#include <stdlib.h>
#define G 10

// Simone Tugnetti

int vettore[G],i;

/*
	Funzione che carica l'array con 10 elementi richiesti all'utente
*/
void carica(){
	for(i=0;i<G;i++){
		printf("\nInserisci l'elemento numero %d --> ",i);
		scanf("%d",&vettore[i]);
	}
}

/*
	Funzione che stampa l'intero array
*/
void stampa(){
	printf("\nElementi nel vettore --> ");
	for(i=0;i<G;i++){
		printf("%d, ",vettore[i]);
	}
	printf("\b\b.");
}

/*
	Funzione che stampa l'indice dell'array con il valore massimo
	@RETURN indice del valore massimo
*/
int indiceMAX(){
	int MAX=vettore[0],k=0;
	for(i=0;i<G;i++){
		if(vettore[i]>MAX){
			MAX=vettore[i];
			k=i;
		}
	}
	return k;
}

/*
	Funzione che stampa l'indice dell'array con il valore minimo
	@RETURN indice del valore minimo
*/
int indiceMIN(){
	int MIN=vettore[0],k=0;
	for(i=0;i<G;i++){
		if(vettore[i]<MIN){
			MIN=vettore[i];
			k=i;
		}
	}
	return k;
}

/*
	Funzione che stampa la somma dei valori nell'array
*/
void somma(){
	int somma=0;
	for(i=0;i<G;i++){
		somma=somma+vettore[i];
	}
	printf("\nSomma dei valori --> %d",somma);
}

/*
	Funzione che scambia due valori nelle posizioni prefissate
	@PARAM posizione numero 1 e posizione numero 2
*/
void scambio(int x, int y){
	int temp;
	temp=vettore[x];
	vettore[x]=vettore[y];
	vettore[y]=temp;
}

/*
	Funzione che ordina l'intero array in ordine crescente
*/
void ordinamento(){
	int j;
	printf("\nL'array verra' ordinato!");
	for(i=0;i<G;i++){
		for(j=i+1;j<G;j++){
			if(vettore[i]>vettore[j]){
				scambio(i,j);
			}
		}
	}
}

int main(int argv, char *argc[]){
	int x,y;
	carica();
	stampa();
	printf("\nIndice con il valore Massimo --> %d",indiceMAX());
	printf("\nIndice con il valore Minimo --> %d",indiceMIN());
	somma();
	printf("\nInserisci la prima posizione --> ");
	scanf("%d",&x);
	while(x<0 || x>G-1){
		printf("\nValore non valido, riprova --> ");
		scanf("%d",&x);
	}
	printf("\nInserisci la seconda posizione --> ");
	scanf("%d",&y);
	while(y<0 || y>G-1){
		printf("\nValore non valido, riprova --> ");
		scanf("%d",&y);
	}
	scambio(x,y);
	stampa();
	ordinamento();
	stampa();
}

