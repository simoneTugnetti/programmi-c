#include <stdlib.h>
#include <stdio.h>

// Simone Tugnetti

/*
Crea un numero non oltre i 100 milioni che pu� essere interrotto
premendo Invio
@RETURN Valore intero dell'insieme delle cifre inserite in
precedenza
*/
int leggiNumero(){
	int Num=0,Val,C;
	
	printf("\nInserisci il valore --> ");
	Val=leggiCifra();
	
	for(C=0;C<9 && Val>=0;C++){
		Num=(Num*10)+Val;
		Val=leggiCifra();
	}
	
	return Num;
}

/*
Chiede di inserire una tra le 4 operazioni +, *, -, /
@RETURN L'operazione desiderata
*/
char Operazione(){
	char op;
	
	op=getch();
	while(op!='+' && op!='-' && op!='*' && op!='/'){
		op=getch();
	}
	
	printf("%c",op);

	return op;
}

/*
Calcola 2 numeri con un'operazione a scelta tra +, -, *, /
@RETURN Il risultato dell'operazione
*/
float Calcolatrice(){
	char op;
	float Num1,Num2,Tot=0;
	
	Num1=(float)leggiNumero();
	printf("\nInserisci l'operazione --> ");
	op=Operazione();
	Num2=(float)leggiNumero();
	
	if(op=='+'){
		Tot=Num1+Num2;
	}else if(op=='-'){
		Tot=Num1-Num2;
	}else if(op=='*'){
		Tot=Num1*Num2;
	}else if(op=='/'){
		if(Num2!=0){
			Tot=Num1/Num2;
		}
	}
	
	return Tot;
}

int main(int argc, char *argv[]){
	float Tot;
	Tot=Calcolatrice();
	printf("\n%f",Tot);
}

