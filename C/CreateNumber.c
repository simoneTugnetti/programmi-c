#include <stdlib.h>
#include <stdio.h>
#define Invio 13

// Simone Tugnetti

/*
Funzione che legge un carattere tra 0 e 9 (oppure Invio)
@RETURN cifra convertita nel suo corrispondente valore intero
*/
int leggiCifra(){
	char cifra;
	int cifraIntera;
	
	cifra=getch();
	while((cifra<'0' || cifra>'9') && cifra!=Invio){
		cifra=getch();
	}
	
	printf("%c",cifra);
	cifraIntera=cifra-'0';
	
	return cifraIntera;

}

/*
Crea un numero non oltre i 100 milioni che pu� essere interrotto
premendo Invio
@RETURN Valore intero dell'insieme delle cifre inserite in
precedenza
*/
int leggiNumero(){
	int Num=0,Val,C;
	
	printf("\nInserisci il valore --> ");
	Val=leggiCifra();
	
	for(C=0;C<9 && Val>=0;C++){
		Num=(Num*10)+Val;
		Val=leggiCifra();
	}
	
	return Num;

}

int main(int argc, char *argv[]){
	int Num;
	Num=leggiNumero();
	printf("\n%d",Num);
}

