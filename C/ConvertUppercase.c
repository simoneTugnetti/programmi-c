#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int main(int argc, char *argv[]){
	char Car;
	
	printf("Inserisci una lettera Maiuscola o minuscola --> ");
	Car=getch();
	printf("%c",Car);

	while((Car<'a' || Car>'z') && (Car<'A' || Car>'Z')){
		printf("\nValore non valido, riprova --> ");
		Car=getch();
		printf("%c",Car);
	}
	
	if(Car>='a' && Car<='z'){
		Car=Car-('a'-'A');
	}
	
	printf("\nQuesta e' la lettera in Maiuscolo --> %c",Car);

}
