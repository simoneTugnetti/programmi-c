#include <stdlib.h>
#include <stdio.h>

// Simone Tugnetti

int main(int argc, char *argv[]){
	int C=0,Div=1,Num;
	
	printf("Inserisci un valore maggiore di 0 --> ");
	scanf("%d",&Num);

	while(Num<=0){
		printf("Valore non valido, riprova --> ");
		scanf("%d",&Num);
	}
	
	while(Div<Num){
		if(Num%Div==0){
			C=C+Div;
		}
		Div++;
	}
	
	if(C==Num){
		printf("Il Valore e' un numero perfetto!");
	}else{
		printf("Il Valore non e' un numero perfetto!");
	}
	
}
