#include <stdlib.h>
#include <stdio.h>

// Simone Tugnetti

int main(int argc, char *argv[]){
	int Num;
	
	printf("Inserisci un valore decimale --> ");
	scanf("%d",&Num);

	while(Num<0){
		printf("Valore non valido, riprova --> ");
		scanf("%d",&Num);
	}
	
	if(Num==0){
		printf("%d",Num);
	}else{
		while(Num>0){
			printf("%d",Num%2);
			Num=Num/2;
		}
	}
	
}
