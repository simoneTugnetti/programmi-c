#include <stdlib.h> 
#include <stdio.h> 
#define Invio 13

// Simone Tugnetti

/*
Funzione che legge un carattere tra 0 e 9 o tra a e f (minuscolo o
Maiuscolo) (oppure Invio)
@RETURN cifra convertita nel suo corrispondente valore intero
*/
int leggiCifra(){
	char cifra;
	int cifraIntera;
	
	cifra=getch();
	while((cifra<'0' || cifra>'9') && (cifra<'a' || cifra>'f') &&
		(cifra<'A' || cifra>'F') && cifra!=Invio){

		cifra=getch();
	}

	if(cifra>='a' && cifra<='f'){
		cifra=cifra-('a'-'A');
	}
	
	printf("%c",cifra);
	if((cifra>='0' && cifra<='9') || cifra==Invio){
		cifraIntera=cifra-'0';
	}else if(cifra>='A' && cifra<='F'){
		cifraIntera=cifra-'7';
	}
	
	return cifraIntera;
}

/*
Crea un numero non oltre il valore 16^6 che pu� essere interrotto
premendo Invio
@RETURN Valore intero dell'insieme delle cifre inserite in precedenza
*/
int leggiNumero(){
	int Num=0,Val,C,Esp=1;
	
	printf("Inserisci il valore --> ");
	Val=leggiCifra();
	
	for(C=0;C<7 && Val>=0;C++){
		Num=Num+(Val*Esp);
		Esp=Esp*16;
		Val=leggiCifra();
	}
	
	return Num;
}

int main(int argc, char *argv[]){
	int Num;
	Num=leggiNumero();
	printf("\n%d",Num); 
}

