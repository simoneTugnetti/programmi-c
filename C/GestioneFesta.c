#include <stdlib.h>
#include <stdio.h>
#define DIM 5

// Simone Tugnetti

/**
Chiede di inserire una lettera minuscola o Maiuscola
@RETURN Corrispettivo Maiuscolo di quella lettera
*/
char MinuToMaiu(){
	char lettera;
	lettera=getch();
	while((lettera<'a' || lettera>'z') && (lettera<'A' || lettera>'Z')){
		lettera=getch();
	}
	if(lettera>='a' && lettera<='z'){
		lettera=lettera-('a'-'A');
	}
	printf("%c\n",lettera);
	return lettera;
}

/**
Funzione che carica l'array con il nome, l'et� ed il costume
@PARAM vettore dei nomi, dell'et� e del costume degli invitati e
la dimensione dei vettori
@MODIFY L'array viene caricato con tutti i dati
*/
void carica(char n[], int e[], char c[], int dim){
	int i;
	for(i=0;i<dim;i++){
		printf("\nInserisci l'iniziale del nome dell'invitato %d --> ",i);
		n[i]=MinuToMaiu();
		printf("\nInserisci l'eta' dell'invitato %d --> ",i);
		scanf("%d",&e[i]);
		printf("\nInserisci l'iniziale del costume che indossera' l'invitato %d --> ",i);
		c[i]=MinuToMaiu();
	}
}

/**
Funzione che stampa i dati di ogni invitato
@PARAM vettore dei nomi, dell'et� e del costume degli invitati e
la dimensione dei vettori
*/
void stampa(char n[], int e[], char c[], int dim){
	int i;
	printf("\nNome degli invitati --> ");
	for(i=0;i<dim;i++){
		printf("%c, ",n[i]);
	}
	printf("\b\b.");
	printf("\nEta' degli invitati --> ");
	for(i=0;i<dim;i++){
		printf("%d, ",e[i]);
	}
	printf("\b\b.");
	printf("\nCostume degli invitati --> ");
	for(i=0;i<dim;i++){
		printf("%c, ",c[i]);
	}
	printf("\b\b.");
}

/**
Funzione che calcola e stampa la media di tutti gli invitati
aventi un determinato costume
@PARAM vettore dell'et� e del costume degli invitati e la
dimensione dei vettori
@RETURN media delle rispettive eta'
*/
float media(int e[], char c[], int dim){
	int i,somma=0,j=0;
	float media=-1;
	char costume;
	printf("\nInserisci l'iniziale del costume --> ");
	costume=MinuToMaiu();
	for(i=0;i<dim;i++){
		if(c[i]==costume){
			somma=somma+e[i];
			j++;
		}
	}
	if(j>0){
		media=(float)somma/j;
	}
	return media;
}

/**
Funzione che stampa i nomi di tutte le persone che indosseranno
un determinato costume
@PARAM vettore dei nomi e dei costumi degli invitati e dimensione
del vettore
*/
void stampaNomeCost(char n[], char c[], int dim){
	int i,trov=0;
	char costume;
	printf("\nInserisci l'iniziale del costume --> ");
	costume=MinuToMaiu();
	for(i=0;i<dim;i++){
		if(c[i]==costume){
			printf("\nNome dell'invitato --> %c",n[i]);
			trov=1;
		}
	}
	if(trov==0){
		printf("\nNon e' stato trovato alcun invitato!");
	}
}

/**
Funzione che calcola l'eta' minima degli invitati
@PARAM vettore dell'eta' degli invitati e dimensione del vettore
@RETURN l'eta' minima di tutti gli invitati
*/
int etaMinima(int e[], int dim){
	int MIN=e[0],i;
	for(i=1;i<dim;i++){
		if(e[i]<MIN){
			MIN=e[i];
		}
	}
	return MIN;
}

/**
Funzione che calcola l'eta' massima degli invitati
@PARAM vettore dell'eta' degli invitati e dimensione del vettore
@RETURN l'eta' massima di tutti gli invitati
*/
int etaMassima(int e[], int dim){
	int MAX=e[0],i;
	for(i=1;i<dim;i++){
		if(e[i]>MAX){
			MAX=e[i];
		}
	}
	return MAX;
}

/**
Funzione che stampa della persona piu' anziana che indossera' un
determinato costume
@PARAM vettore dei nomi, dell'eta' e dei costumi degli invitati e
dimensione del vettore
*/
void stampaAnziano(char n[], int e[], char c[], int dim){
	int i,trov=0,MAX=0,imax=0;
	char costume;
	printf("\nInserisci l'iniziale del costume --> ");
	costume=MinuToMaiu();
	for(i=0;i<dim;i++){
		if(c[i]==costume && e[i]>MAX){
			MAX=e[i];
			imax=i;
			trov=1;
		}
	}
	if(trov==0){
		printf("\nNon e' stato trovato alcun invitato!");
	}else{
		printf("\nNome dell'invitato --> %c",n[imax]);
		printf("\nEta' dell'invitato --> %d",e[imax]);
		printf("\nCostume dell'invitato --> %c",c[imax]);
	}
}

/**
Funzione che stampa i dati di un determinato invitato tramite il
nome
@PARAM vettore dei nomi, dell'eta' e dei costumi degli invitati e
dimensione del vettore
*/
void stampaDatiNome(char n[], int e[], char c[], int dim){
	int i,trov=0;
	char nome;
	printf("\nInserisci l'iniziale del nome --> ");
	nome=MinuToMaiu();
	for(i=0;i<dim;i++){
		if(n[i]==nome){
			printf("\nNome dell'invitato --> %c",n[i]);
			printf("\nEta' dell'invitato --> %d",e[i]);
			printf("\nCostume dell'invitato --> %c",c[i]);
			trov=1;
		}
	}
	if(trov==0){
		printf("\nNon e' stato trovato alcun invitato!");
	}
}

/**
Funzione che ordina tutti i vettore in base al nome in ordine
alfabetico crescente
@PARAM vettore dei nomi, dell'eta' e dei costumi degli invitati e
dimensione del vettore
@MODIFY Tutti i vettori risulteranno ordinati
*/
void ordinamento(char n[], int e[], char c[], int dim){
	int i,j,temp;
	char tempc;
	for(i=0;i<dim;i++){
		for(j=i+1;j<dim;j++){
			if(n[i]>n[j]){
				tempc=n[i];
				n[i]=n[j];
				n[j]=tempc;
				temp=e[i];
				e[i]=e[j];
				e[j]=temp;
				tempc=c[i];
				c[i]=c[j];
				c[j]=tempc;
			}
		}
	}
}

/**
Funzione che stampa l'elenco contenente tutti i costumi che
saranno indossati e per ognuno di essi il numero di persone che lo
indosseranno
@PARAM vettore dei costumi degli invitati e dimensione del
vettore
*/
void stampaElencoCost(char c[], int dim){
	int i,trov=1,somma;
	char cost[dim],costume;
	for(i=0;i<dim;i++){
		cost[i]=c[i];
	}
	costume=cost[0];
	while(trov==1){
		somma=0;
		for(i=0;i<dim;i++){
			if(cost[i]==costume){
				somma++;
				cost[i]='0';
			}
		}
		printf("\nNumero di invitati aventi il costume %c --> %d",costume,somma);
		trov=0;
		for(i=0;i<dim && trov==0;i++){
			if(cost[i]!='0'){
				costume=cost[i];
				trov=1;
			}
		}
	}
}

int main(int argv, char *argc[]){
	int eta[DIM],scelta=1;
	char nome[DIM], costume[DIM];
	float Media;
	carica(nome,eta,costume,DIM);
	while(scelta!=0){
		printf("\n1 --> Stampa i dati degli invitati!");
		printf("\n2 --> Stampa degli invitati che indossano un determinato costume!");
		printf("\n3 --> Calcolo dell'eta' media degli invitati che indossano un determinato costume!");
		printf("\n4 --> Calcolo della fascia d'eta' degli invitati!");
		printf("\n5 --> Stampa della persona piu' anziana che indossa un determinato costume!");
		printf("\n6 --> Stampa i dati di un determinato invitato!");
		printf("\n7 --> Ordinamento di tutti i dati in ordine alfabetico crescente del nome!");
		printf("\n8 --> Stampa dell'elenco di ogni costume con numero di persone che lo indosseranno!");
		printf("\n0 --> Esci");
		printf("\n\nIntroduci la scelta --> ");
		scanf("%d",&scelta);
		switch(scelta){
			case 1:
				stampa(nome,eta,costume,DIM);
				printf("\n");
				break;
			case 2:
				stampaNomeCost(nome,costume,DIM);
				printf("\n");
				break;
			case 3:
				Media=media(eta,costume,DIM);
				if(Media==-1){
					printf("\nNon e' stato trovato alcun invitato!");
				}else{
					printf("\nMedia dell'eta' degli invitati --> %f\n",Media);
				}
				break;
			case 4:
				printf("\nLa fascia d'eta' degli invitati va da %d a %d anni.\n",etaMinima(eta,DIM),etaMassima(eta,DIM));
				break;
			case 5:
				stampaAnziano(nome,eta,costume,DIM);
				printf("\n");
				break;
			case 6:
				stampaDatiNome(nome,eta,costume,DIM);
				printf("\n");
				break;
			case 7:
				ordinamento(nome,eta,costume,DIM);
				printf("\n");
				break;
			case 8:
				stampaElencoCost(costume,DIM);
				printf("\n");
				break;
			case 0:
				printf("\nGrazie per aver usato il nostro programma!");
				break;
			default:
				printf("\nOpzione non valida, riprova!\n");
				break;
		}
	}
	
}

