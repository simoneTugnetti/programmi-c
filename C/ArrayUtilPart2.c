#include <stdlib.h>
#include <stdio.h>
#define DIM 8

// Simone Tugnetti

/**
Chiede di inserire una lettera minuscola o Maiuscola
@RETURN lettera inserita
*/
char InsLettera(){
	char lettera;
	lettera=getch();
	while((lettera<'a' || lettera>'z') && (lettera<'A' ||
		lettera>'Z')){
		lettera=getch();
	}
	printf("%c\n",lettera);
	return lettera;
}

/**
Funzione che carica gli array con una lettera inserita in input
@PARAM vettore della prima parola, vettore della seconda parola e
la dimensione dell'array
@MODIFY Gli array vengono caricati con le lettere inserite
*/
void carica(char p1[], char p2[], int dim){
	int i;
	for(i=0;i<dim;i++){
		printf("\nInserisci la lettera numero %d della parola numero 1 --> ",i);
		p1[i]=InsLettera();
	}
	for(i=0;i<dim;i++){
		printf("\nInserisci la lettera numero %d della parola numero 2 --> ",i);
		p2[i]=InsLettera();
	}
}

/**
Funzione che stampa i caratteri di ogni array
@PARAM vettore della prima parola, vettore della seconda parola e
la dimensione dell'array
*/
void stampa(char p1[], char p2[], int dim){
	int i;
	printf("\nPrima parola --> ");
	for(i=0;i<dim;i++){
		printf("%c ",p1[i]);
	}
	printf("\nSeconda parola --> ");
	for(i=0;i<dim;i++){
		printf("%c ",p2[i]);
	}
}

/**
Funzione che verifica se i caratteri all'interno dei due array
siano uguali nelle stesse posizioni
@PARAM vettore della prima parola, vettore della seconda parola e
la dimensione degli array
@RETURN variabile che verifica se sono uguali oppure no
*/
int Uguali(char p1[], char p2[], int dim){
	int i, trov=0;
	for(i=0;i<dim;i++){
		if(p1[i]!=p2[i]){
			trov=1;
		}
	}
	return trov;
}

/**
Funzione che verifica se i caratteri all'interno dei due array
siano uguali nelle stesse posizioni a parte le Maiuscole e le minuscole
@PARAM vettore della prima parola, vettore della seconda parola e
la dimensione degli array
@RETURN variabile che verifica se sono uguali oppure no
*/
int UgualiNoMaiuMinu(char p1[], char p2[], int dim){
	int i, trov=0;
	char pa1[dim],pa2[dim];
	for(i=0;i<dim;i++){
		pa1[i]=p1[i];
		if(pa1[i]>='a' && pa1[i]<='z'){
			pa1[i]=pa1[i]-('a'-'A');
		}
		pa2[i]=p2[i];
		if(pa2[i]>='a' && pa2[i]<='z'){
			pa2[i]=pa2[i]-('a'-'A');
		}
	}
	for(i=0;i<dim;i++){
		if(pa1[i]!=pa2[i]){
			trov=1;
		}
	}
	return trov;
}

int main(int argv, char *argc[]){
	int scelta=1, trov;
	char parola1[DIM], parola2[DIM];
	carica(parola1,parola2,DIM);
	while(scelta!=0){
		printf("\n1 --> Stampa le due parole!");
		printf("\n2 --> Stampa se le due parole sono uguali!");
		printf("\n3 --> Stampa se le due parole sono uguali a parte le Maiuscole e le minuscole!");
		printf("\n0 --> Esci");
		printf("\n\nIntroduci la scelta --> ");
		scanf("%d",&scelta);
		switch(scelta){
			case 1:
				stampa(parola1,parola2,DIM);
				printf("\n");
				break;
			case 2:
				trov=Uguali(parola1,parola2,DIM);
				if(trov==0){
					printf("\nLe due parole sono uguali!");
				}else{
					printf("\nLe due parole non sono uguali!");
				}
				printf("\n");
				break;
			case 3:
				trov=UgualiNoMaiuMinu(parola1,parola2,DIM);
				if(trov==0){
					printf("\nLe due parole sono uguali!");
				}else{
					printf("\nLe due parole non sono uguali!");
				}
				printf("\n");
				break;
			case 0:
				printf("\nGrazie per aver usato il nostro programma!");
				break;
			default:
				printf("\nOpzione non valida, riprova!\n");
				break;
		}
	}
	
}

