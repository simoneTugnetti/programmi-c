#include <stdlib.h>
#include <stdio.h>

// Simone Tugnetti

/*
Crea un numero non oltre i 100 milioni che pu� essere interrotto
premendo Invio
@RETURN Valore intero dell'insieme delle cifre inserite in
precedenza
*/
int leggiNumero(){
	int Num=0,Val,C;
	
	printf("\nInserisci il valore --> ");
	Val=leggiCifra();
	
	for(C=0;C<9 && Val>=0;C++){
		Num=(Num*10)+Val;
		Val=leggiCifra();
	}
	
	return Num;
}

/*
Calcola il minimo tra due valori
@RETURN Minimo valore riconosciuto
*/
int Minimo(int Min, int Num){
	if(Num<Min){
		Min=Num;
	}

	return Min;
}

/*
Calcola il massimo tra due valori
@RETURN Massimo valore riconosciuto
*/
int Massimo(int Max, int Num){
	if(Num>Max){
		Max=Num;
	}

	return Max;
}

int main(int argc, char *argv[]){
	int C,MAX,MIN,Num;

	printf("Massimo Valore: ");
	MAX=leggiNumero();
	printf("\nE' stato scelto il valore Minimo!");
	MIN=MAX;
	printf("\nConfronti: ");

	for(C=0;C<8;C++){
		Num=leggiNumero();
		MIN=Minimo(MIN,Num);
		MAX=Massimo(MAX,Num);
	}
	
	printf("\nValore Minimo: %d",MIN);
	printf("\nValore Massimo: %d",MAX);
}

