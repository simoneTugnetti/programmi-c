#include <stdio.h>
#include <stdlib.h>

// Simone Tugnetti

int main(int argc, char *argv[]){
	int C=0,D,Num=2,Div;

	printf("Ecco la lista dei primi 100 numeri primi --> ");
		
	while(C<100){
		D=0;
		Div=2;
		
		while(Div<Num && D==0){
			if(Num%Div==0){
				D=1;
			}
			Div++;
		}
		
		if(D==0){
			printf("%d, ",Num);
			C++;
		}
		Num++;
	}

	printf("\b\b.");

}
