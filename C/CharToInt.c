#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int main(int argc, char *argv[]){
	char Car;
	int Intero;

	printf("Inserisci una cifra decimale --> ");
	Car=getch();

	while(Car<'0' || Car>'9'){
		printf("\nValore non valido, riprova --> ");
		Car=getch();
		printf("%c",Car);
	}
	
	Intero=Car-'0';
	printf("\nEcco la cifra nella variabile intera --> %d",Intero);

}
