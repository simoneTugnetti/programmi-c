#include <stdlib.h>
#include <stdio.h>
#define DIM 50
#define Invio 13

// Simone Tugnetti

/**
Chiede di inserire una lettera minuscola o Maiuscola, l'invio o
lo spazio
@RETURN carattere inserito
*/
char InsLettera(){
	char lettera;
	lettera=getch();
	while((lettera<'a' || lettera>'z') && (lettera<'A' || lettera>'Z') && lettera!=Invio && lettera!=' '){
		lettera=getch();
	}
	printf("%c",lettera);
	return lettera;
}

/**
Crea una parola non oltre i 50 caratteri
@PARAM vettore della frase da inserire e la dimensione del
vettore
@RETURN Ritorna la grandezza massima dell'array in base alle
lettere inserite
*/
int leggiParola(char parola[], int dim){
	char Val;
	int i;
	printf("\nInserisci la frase --> ");
	Val=InsLettera();
	for(i=0;i<dim && Val!=Invio;i++){
		parola[i]=Val;
		Val=InsLettera();
	}
	return i;
}

/**
Funzione che stampa la frase
@PARAM vettore della frase e la dimensione del vettore
*/
void stampa(char parola[], int dim){
	int i;
	printf("\nEcco la frase --> ");
	for(i=0;i<dim;i++){
		printf("%c",parola[i]);
	}
	printf(".");
}

/**
Funzione che stampa l'elenco di tutte le lettere e la loro
quantit� in una frase
@PARAM vettore delle lettere e dimensione del vettore
*/
void stampaElencoLettere(char parola[], int dim){
	int i,somma;
	char letteraM;
	for(letteraM='A';letteraM<='Z';letteraM++){
		somma=0;
		for(i=0;i<dim;i++){
			if(parola[i]==letteraM || parola[i]==(letteraM+('a'-'A'))){
				somma++;
			}
		}
		printf("\nLettera %c --> %d",letteraM,somma);
	}
}

/**
Funzione che verifica se la frase � palindroma
@PARAM vettore della frase e la dimensione del vettore
@RETURN variabile di verifica se � palindroma o no
*/
int palindromo(char parola[], int dim){
	int i,j=dim-1,trov=0;
	char frase[dim];
	for(i=0;i<dim;i++){
		frase[i]=parola[i];
		if(frase[i]>='a' && frase[i]<='z'){
			frase[i]=frase[i]-('a'-'A');
		}
	}
	for(i=0;i<dim && j>=0;i++){
		if(frase[i]!=frase[j]){
			trov=1;
		}
		j--;
	}
	return trov;
}

int main(int argv, char *argc[]){
	int scelta=1,trov;
	char parola[DIM];
	int i=leggiParola(parola,DIM);
	while(scelta!=0){
		printf("\n1 --> Stampa la frase!");
		printf("\n2 --> Stampa l'elenco delle lettere!");
		printf("\n3 --> Verifica se la frase e' palindroma!");
		printf("\n0 --> Esci");
		printf("\n\nIntroduci la scelta --> ");
		scanf("%d",&scelta);
		switch(scelta){
			case 1:
				stampa(parola,i);
				printf("\n");
				break;
			case 2:
				stampaElencoLettere(parola,i);
				printf("\n");
				break;
			case 3:
				trov=palindromo(parola,i);
				if(trov==0){
					printf("La parola e' palindroma!");
				}else{
					printf("La parola non e' palindroma!");
				}
				break;
			case 0:
				printf("\nGrazie per aver usato il nostro programma!");
				break;
			default:
				printf("\nOpzione non valida, riprova!\n");
				break;
		}
	}
	
}

