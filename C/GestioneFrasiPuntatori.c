#include <stdio.h>
#include <stdlib.h>
#define Invio 13
#define Alf 26
#define DIM 50

// Simone Tugnetti

char InsLettera();
int leggiParola(char parola[], int dim);
void stampa(char p1[], int dim);
void minuToMaiu(char *valore);
void Azzeramento(int alfabeto[], int dim);
void rangeLet(char parola[], int alfabeto[], int dim, int dima, char *prima, char *ultima);
void lettRipetuta(int alfabeto[], int dim, int *max, char *carattere);

int main(int argv, char *argc[]){
	char parola[DIM],prima,ultima,carattere='0';
	int alfabeto[Alf],max=0,scelta=1,i;
	Azzeramento(alfabeto,Alf);
	i=leggiParola(parola,DIM);
	while(scelta!=0){
		printf("\n1 --> Stampa la frase!");
		printf("\n2 --> Stampa il range di lettere usate!");
		printf("\n3 --> Stampa la lettera ripetuta piu' volte!");
		printf("\n0 --> Esci");
		printf("\n\nIntroduci la scelta --> ");
		scanf("%d",&scelta);
		switch(scelta){
			case 1:
				stampa(parola,i);
				printf("\n");
				break;
			case 2:
				rangeLet(parola,alfabeto,i,Alf,&prima,&ultima);
				printf("\nLe lettere utilizzate vanno da %c a %c!",prima,ultima);
				printf("\n");
				break;
			case 3:
				lettRipetuta(alfabeto,i,&max,&carattere);
				if(carattere=='0'){
					printf("\nNon ci sono lettere nella frase!");
				}else{
					printf("\nLa lettera ripetuta piu' volte e' la %c --> %d volte",carattere,max);
				}
				break;
			case 0:
				printf("\nGrazie per aver usato il nostro programma!");
				break;
			default:
				printf("\nOpzione non valida, riprova!\n");
				break;
		}
	}
}

/**
Chiede di inserire una lettera minuscola o Maiuscola, l'invio o
lo spazio
@RETURN carattere inserito
*/
char InsLettera(){
	char lettera;
	lettera=getch();
	while((lettera<'a' || lettera>'z') && (lettera<'A' || lettera>'Z') && lettera!=Invio && lettera!=' '){
		lettera=getch();
	}
	printf("%c",lettera);
	return lettera;
}

/**
Crea una parola non oltre i 50 caratteri
@PARAM vettore della frase da inserire e la dimensione del
vettore
@RETURN Ritorna la grandezza massima dell'array in base alle
lettere inserite
*/
int leggiParola(char parola[], int dim){
	char Val;
	int i;
	printf("\nInserisci la frase --> ");
	Val=InsLettera();
	for(i=0;i<dim && Val!=Invio;i++){
		parola[i]=Val;
		Val=InsLettera();
	}
	return i;
}

/**
Funzione che stampa i caratteri di ogni array
@PARAM vettore della prima parola, vettore della seconda parola e
la dimensione dell'array
*/
void stampa(char p1[], int dim){
	int i;
	printf("\nPrima parola --> ");
	for(i=0;i<dim;i++){
		printf("%c ",p1[i]);
	}
}

/**
Funzione che converte una lettera minuscola in maiuscola
@PARAM lettera da convertire
@MODIFY lettera convertita
*/
void minuToMaiu(char *valore){
	if(*valore>='a' && *valore<='z'){
		*valore=*valore-('a'-'A');
	}
}

/**
Funzione che azzera un vettore di interi
@PARAM vettore da azzerare e dimensione dell'array
@MODIFY vettore azzerato
*/
void Azzeramento(int alfabeto[], int dim){
	int i;
	for(i=0;i<dim;i++){
		alfabeto[i]=0;
	}
}

/**
Funzione che calcola il range di lettere utilizzate
@PARAM vettore della frase, vettore dell'alfabeto, dimensione del
vettore della frase, dimensione del vettore dell'alfabeto, variabile
della prima lettera, variabile dell'ultima lettera
@MODIFY Salva la prima lettera trovata assieme all'ultima
*/
void rangeLet(char parola[], int alfabeto[], int dim, int dima, char *prima, char *ultima){
	int i,indice,j,trov=0;
	char carattere;
	Azzeramento(alfabeto,dima);
	for(i=0;i<dim;i++){
		carattere=parola[i];
		minuToMaiu(&carattere);
		indice=carattere-'A';
		if(indice>=0 && indice<dima){
			alfabeto[indice]++;
		}
	}
	for(i=0;i<dima && trov==0;i++){
		if(alfabeto[i]>0){
			trov=1;
			*prima=i+'A';
		}
	}
	trov=0;
	for(j=dima-1;j>=0 && trov==0;j--){
		if(alfabeto[j]>0){
			trov=1;
			*ultima=j+'A';
		}
	}
}

/**
Funzione che trova qual'� la lettera ripetuta pi� volte
@PARAM vettore dell'alfabeto, dimensione del vettore, valore
massimo ripetuto, lettera ripetuta
@MODIFY Salva il valore massimo e la lettera ripetuta
*/
void lettRipetuta(int alfabeto[], int dim, int *max, char *carattere){
	int i;
	for(i=0;i<dim;i++){
		if(alfabeto[i]>*max){
			*max=alfabeto[i];
			*carattere=i+'A';
		}
	}
}

