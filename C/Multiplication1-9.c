#include <stdio.h>
#include <stdlib.h>

// Simone Tugnetti

int main(int argc, char *argv[]){
	int C,Num=2;

	while(Num<10){
		C=1;
		printf("Tabellina del %d --> ",Num);
		
		while(C<11){
			printf("%d ",Num*C);
			C++;
		}
		
		Num++;
		printf("\n");
	}
	
}
