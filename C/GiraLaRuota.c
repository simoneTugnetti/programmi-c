#include <stdio.h>
#include <stdlib.h>
#define INVIO 13
#define MAX_SIZE 200

/**
@AUTHOR: Simone Tugnetti, Razvan Apostol, Christian Palmeri
Titolo: GiraLaRuota
*/

// Prototipi delle funzioni

// Funzione che legge una lettera e lo spazio
char leggiLettera();

// Funzione che legge la frase inserita dal utente
void leggiFraseUtente(char vetUtente[], int dim);

// Funzione che legge la frase segreta
int leggiFraseSegreta(char vetSecret[],int dim);

/* 
Funzione che decurta il
premio di una data percentuale, passandole il valore del monte premi e
ritorna il valore del monte premi aggiornato
*/
float decurtaPremio(float montePremi);

// Funzione che cerca le consonanti al interno del vettore con la frase segreta
int trovConsonante(char frase[], char asterischi[], char consonante, int dim);

// Funzione che richiama il decurta premi e mette una vocale all'interno della frase segreta
void aggiungiVocale(char frase[], char asterischi[], char vocali[], int dim, int *k);

// Funziona che controlla se la frase inserita dal utente � uguale alla frase segreta
int trovaFrase(char vetSecret[], int dim);

// Funzione che stampa il vettore inserito
void stampaVet(char vet[], int dim);

// Funzione che trasforma la frase segreta in asterischi
void asterisco(char vetSecret[], char asterischi[], int dim);

// Funzione che mette le vocali al interno di un vettore del main
void setVocali(char vocali[]);


int main(int argc, char** argv) {
	int k=0,flag,scelta=1,finale=1,i,flagFinale=1;
	float montePremi=1000;
	char vetSecret[MAX_SIZE],vocali[5],consonante;
	int dimensioneFrase = leggiFraseSegreta(vetSecret,MAX_SIZE);
	char vetUtente[dimensioneFrase],asterischi[dimensioneFrase];
	setVocali(vocali);
	asterisco(vetSecret,asterischi,dimensioneFrase);
	printf("\n\nBenvenuti in Gira La Ruota!");
	while(scelta!=0 && finale!=0 && flagFinale!=0){
		printf("\n1 --> Tenta di trovare una consonante!");
		printf("\n2 --> Paga per aggiungere una vocale!");
		printf("\n3 --> Prova a indovinare la frase!");
		printf("\n\nIntroduci la scelta --> ");
		scanf("%d",&scelta);
		switch(scelta){
			case 1:
				printf("\nInserisci una consonante:");
				consonante=leggiLettera();
				while(consonante=='a' || consonante=='e' || consonante=='i' 
						|| consonante=='o' || consonante=='u'){
					printf("\nNon hai inserito una consonante, riprova!");
					consonante=leggiLettera();
				}
				flag=trovConsonante(vetSecret,asterischi,consonante,dimensioneFrase);
				if(flag==1){
					printf("\nSono state trovate delle consonanti\n");
					stampaVet(asterischi, dimensioneFrase);
				}else{
					printf("\nNon sono state trovate delle consonanti");
					montePremi=decurtaPremio(montePremi);
					printf("\nIl nuovo montePremi e' %f",montePremi);
				}
				printf("\n");
				break;
			case 2:
				if(k>4){
					printf("\nTutte le vocali sono state inserite");
				}else{
					montePremi=decurtaPremio(montePremi);
					aggiungiVocale(vetSecret,asterischi,vocali,dimensioneFrase,&k);
					printf("\nE' stata inserita una vocale\n");
					stampaVet(asterischi,dimensioneFrase);
					printf("\nIl nuovo montePremi e' %f",montePremi);
				}
				printf("\n");
				break;
			case 3:
				flagFinale=trovaFrase(vetSecret,dimensioneFrase);
				if(flagFinale==1){
					printf("\nNon hai indovinato la frase");
					montePremi=decurtaPremio(montePremi);
				}else{
					printf("\nComplimenti hai indovinato la frase!\n Ecco a te il MONTE PREMI:%f",montePremi);
				}
				printf("\n");
				break;
			default:
				printf("\nOpzione non valida, riprova!\n");
				break;
		}
		finale=0;
		for(i=0;i<dimensioneFrase;i++){
			if(asterischi[i]!=vetSecret[i]){
				finale=1;
			}
		}
	}
	printf("\nAutori: Razvan Apostol, Simone Tugnetti, Cristian Palmeri");
}

/**
Funzione che legge una lettera inserita dal utente, per adesso le
lettere possono essere soltanto minuscole
@RETURN la lettera minuscola
*/
char leggiLettera(){
	char lettera;
	lettera=getch();
	while((lettera<'a' || lettera>'z') && lettera!=INVIO && lettera!=' '){
		lettera=getch();
	}
	return lettera;
}

/**
Funzione che legge la combinazione segreta inserite dal utente
@PARAM vettore della frase segreta, dimensione del vettore
massimo (200)
@MODIFY vettore della frase aggiornata
@RETURN la nuova dimensione della leggiFraseSegreta
*/
int leggiFraseSegreta(char vetSecret[], int dim){
	int i;
	char lettera;
	printf("\nInserisci la combinazione segreta: ");
	lettera=leggiLettera();
	for(i=0;i<dim && lettera!=INVIO;i++){
		if(lettera==' '){
			printf(" ");
		}else{
			printf("*");
		}
		vetSecret[i]=lettera;
		lettera=leggiLettera();
	}
	return i;
}

/**
Funzione che legge la combinazione del utente
@PARAM vettore visibile su schermo (tentativo), dimensione del
vettore (4)
*/
void leggiFraseUtente(char vetUtente[], int dim){
	int i;
	char lettera;
	printf("\nInserisci il tentativo: ");
	lettera=leggiLettera();
	for(i=0;i<dim && lettera!=INVIO;i++){
		printf("%c",lettera);
		vetUtente[i]=lettera;
		lettera=leggiLettera();
	}
}

/**
Funzione stampa un vettore
@PARAM vettore da stamapre, dimensione del vettore
*/
void stampaVet(char vet[], int dim){
	int i;
	for(i=0;i<dim;i++){
		printf("%c",vet[i]);
	}
}

/**
Funzione che controlla se l'utente ha inserito una consonante
presente o meno
@PARAM vettore della frase segreta, vettore per le lettere
indovinate, consonante e dimensioni del vettore
@MODIFY vettore delle lettere indovinate aggiornato
@RETURN valore se � stata trovata una consonante o meno
*/
int trovConsonante(char frase[], char asterischi[], char consonante, int dim){
	int i,trov=0;
	for(i=0;i<dim;i++){
		if(frase[i]==consonante){
			asterischi[i]=consonante;
			trov=1;
		}
	}
	return trov;
}

/**
Funzione che aggiunge una vocale alla frase ausiliaria pagando un
prezzo
@PARAM vettore della frase segreta, vettore per le lettere
indovinate, vettore delle vocali, dimensioni del vettore, indice delle
vocali
@MODIFY vettore delle lettere indovinate aggiornato
*/
void aggiungiVocale(char frase[], char asterischi[], char vocali[], int dim, int *k){
	int i;
	for(i=0;i<dim;i++){
		if(frase[i]==vocali[*k]){
			asterischi[i]=vocali[*k];
		}
	}
	*k=*k+1;
}

/**
Funzione che crea un vettore di vocali
@PARAM vettore delle vocali
*/
void setVocali(char vocali[]){
	vocali[0]='a';
	vocali[1]='e';
	vocali[2]='i';
	vocali[3]='o';
	vocali[4]='u';
}

/**
Funzione che decurta il premio di una data percentuale
@PARAM float montePremi
@RETURN il valore float montePremi aggiornato
*/
float decurtaPremio(float montePremi){
	int i,percentuale=10,totMontePremi;
	percentuale=10;
	totMontePremi = (montePremi*percentuale)/100;
	montePremi = montePremi-totMontePremi;
	return montePremi;
}

/**
Funzione che verifica se l'utente ha inserito la frase uguale a
quella segreta
@PARAM vettore vetSecret e la sua dimensione
@MODIFY vettore vetUtente leggendo la frase inserita dall'utente
@RETURN flag se ha trovato o meno la frase (1 -> non trovato
invece 0 -> trovato)
*/
int trovaFrase(char vetSecret[], int dim){
	int i,flag=0;
	char vetUtente[dim];
	leggiFraseUtente(vetUtente, dim);
	for(i=0; i<dim; i++){
		if(vetUtente[i]!=vetSecret[i]){
			flag=1;
		}
	}
	return flag;
}

/**
Funzione che carica in un vettore asterischi solo per le lettere,
nel caso del carattere spazio inserisce uno spazio
@PARAM vettore vetSecret, vettore asterischi e la dimensione di
entrambi
@MODIFY vettore asterischi con caratteri asterisco o/e spazio
*/
void asterisco(char vetSecret[], char asterischi[], int dim){
	int i;
	for(i=0; i<dim; i++){
		if(vetSecret[i]!=' '){
			asterischi[i]='*';
		}else{
			asterischi[i]=' ';
		}
	}
}

