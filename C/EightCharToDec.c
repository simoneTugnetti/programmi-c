#include <stdlib.h>
#include <stdio.h>

// Simone Tugnetti

int main(int argc, char *argv[]){
	int Num=0,C=0,Val=0;

	while(C<8 && Val>=0){
		Val=leggiCifra();
		
		if(Val>=0){
			Num=(Num*10)+Val;
		C++;
		}
	}
	
	printf("\nQuesto e' il valore decimale: %d",Num);

}

int leggiCifra(){
	char cifra;
	int cifraIntera;
	
	printf("\nInserisci la cifra da 0 a 9: ");
	cifra=getch();
	printf("%c",cifra);

	//Il valore 13 e' il valore intero del pulsante Invio
	while((cifra<'0' || cifra>'9') && cifra!=13){
		printf("\nValore non valido riprova: ");
		cifra=getch();
		printf("%c",cifra);
	}
	
	cifraIntera=cifra-'0';
	
	return cifraIntera;

}
