#include <stdlib.h>
#include <stdio.h>
#define DIM 5

// Simone Tugnetti

/*
Funzione che carica l'array con i prezzi degli smartphone per
ogni distributore
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
@MODIFY L'array viene caricato con tutti i prezzi
*/
void carica(int v[], int dim){
	int i;
	for(i=0;i<dim;i++){
		printf("\nInserisci il prezzo dello smartphone nel distributore %d --> ",i);
		scanf("%d",&v[i]);
	}
}

/*
Funzione che stampa i prezzi degli smartphone
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
*/
void stampa(int v[], int dim){
	int i;
	printf("\nPrezzi degli smartphone --> ");
	for(i=0;i<dim;i++){
		printf("$%d, ",v[i]);
	}
	printf("\b\b.");
}

/*
Funzione che calcola e stampa la media di tutti i prezzi degli
smartphone
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
@RETURN media dei prezzi
*/
float media(int v[], int dim){
	int i,somma=0;
	float media=0;
	for(i=0;i<dim;i++){
		somma=somma+v[i];
	}
	media=(float)somma/dim;
	return media;
}

/*
Funzione che stampa il prezzo dello smartphone e il numero del
suo distributore se quest'ultimi sono minori rispetto ad un'altro
prezzo inserito in input
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
*/
void stampaDist(int v[], int dim){
	int i,preMin,trov=0;
	printf("\nInserisci il prezzo minimo da confrontare --> ");
	scanf("%d",&preMin);
	for(i=0;i<dim;i++){
		if(v[i]<preMin){
			printf("\nDistributore numero --> %d",i);
			printf("\nPrezzo dello smartphone --> $%d",v[i]);
			trov=1;
		}
	}
	if(trov==0){
		printf("\nNon e' stato trovato alcuno smartphone!");
	}
}

/*
Funzione che calcola il prezzo dello smartphone pi� economico
rispetto a tutti gli altri distributori
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
@RETURN indice del distributore dove � presente il prezzo minimo
*/
int prezzoMinimo(int v[], int dim){
	int MIN=v[0],imin=0,i;
	for(i=1;i<dim;i++){
		if(v[i]<MIN){
			MIN=v[i];
			imin=i;
		}
	}
	return imin;
}

/*
Funzione che calcola e stampa il prezzo massimo dello smartphone
fra tutti i prezzi inferiori al prezzo fornito in input
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
@RETURN indice del distributore dove � presente il prezzo massimo
*/
int prezzoMassimo(int v[], int dim){
	int MIN,MAX=0,imax=dim,preMin,i;
	printf("\nInserisci il prezzo minimo da confrontare --> ");
	scanf("%d",&preMin);
	MIN=preMin;
	for(i=0;i<dim;i++){
		if(v[i]<MIN && v[i]>MAX){
			MAX=v[i];
			imax=i;
		}
	}
	return imax;
}

/*
Funzione che stampa il numero del distributore che ha un prezzo
inserito in input, se il distributore non esiste verr� stampato un
messaggio di errore
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
*/
void stessoPrezzo(int v[], int dim){
	int prezzo,trov,i;
	printf("\nInserisci il prezzo da confrontare --> ");
	scanf("%d",&prezzo);
	for(i=0;i<dim;i++){
		if(v[i]==prezzo){
			printf("\nDistributore numero --> %d",i);
			trov=1;
		}
	}
	if(trov==0){
		printf("\nNon e' stato trovato alcuno smartphone con quel prezzo!");
	}
}

/*
Funzione che modifica il prezzo del distributore fornito in input
con un nuovo prezzo dello smartphone sempre fornito in input
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
@MODIFY E' stato modificato il prezzo dello smartphone nel
distributore specifico
*/
void modificaPrezzo(int v[], int dim){
	int prezzo,i;
	printf("\nInserisci il numero del distributore --> ");
	scanf("%d",&i);
	while(i<0 || i>dim-1){
		printf("\nValore non valido, riprova --> ");
		scanf("%d",&i);
	}
	printf("\nInserisci il nuovo prezzo da modificare --> ");
	scanf("%d",&prezzo);
	v[i]=prezzo;
}

/*
Funzione che aumento i prezzi di tutti gli smartphone nei
distributori della percentuale fornita in input
@PARAM vettore dei prezzi degli smartphone e dimensione del
vettore
@MODIFY E' stato applicato l'aumento dei prezzi per tutti gli
smartphone
*/
void modificaPercentuale(int v[], int dim){
	int percentuale,i;
	printf("\nInserisci la percentuale da attuare --> ");
	scanf("%d",&percentuale);
	for(i=0;i<dim;i++){
		v[i]=v[i]+((v[i]*percentuale)/100);
	}
}

int main(int argv, char *argc[]){
	int vet[DIM],scelta=1,PrezzoMassimo=0,PrezzoMinimo=0;
	while(scelta!=0){
		printf("\n1 --> Inserisci i prezzi dei vari smartphone!");
		printf("\n2 --> Stampa i prezzi degli smartphone e i loro distributori!");
		printf("\n3 --> Calcola la media dei prezzi di tutti gli smartphone!");
		printf("\n4 --> Stampa il prezzo minimo dello smartphone e il numero del suo distributore rispetto ad un'altro prezzo!");
		printf("\n5 --> Stampa il prezzo dello smartphone piu' economico rispetto a tutti gli altri distributori!");
		printf("\n6 --> Stampa il prezzo massimo dello smartphone fra tutti i prezzi inferiori ad un prezzo in input!");
		printf("\n7 --> Stampa il numero del distributore che ha lo stesso prezzo inserito in input!");
		printf("\n8 --> Modifica di un prezzo in un determinato distributore!");
		printf("\n9 --> Aumento dei prezzi di tutti gli smartphone nei distributori della percentuale fornita in input!");
		printf("\n0 --> Esci");
		printf("\n\nIntroduci la scelta --> ");
		scanf("%d",&scelta);
		switch(scelta){
			case 1:
				carica(vet, DIM);
				printf("\n");
				break;
			case 2:
				stampa(vet, DIM);
				printf("\n");
				break;
			case 3:
				printf("\nMedia di tutti i prezzi --> $%f\n",media(vet,DIM));
				break;
			case 4:
				stampaDist(vet,DIM);
				printf("\n");
				break;
			case 5:
				PrezzoMinimo=prezzoMinimo(vet,DIM);
				printf("\nDistributore dello smartphone piu' economico --> %d",PrezzoMinimo);
				printf("\nPrezzo dello smartphone piu' economico --> $%d\n",vet[PrezzoMinimo]);
				break;
			case 6:
				PrezzoMassimo=prezzoMassimo(vet,DIM);
				if(PrezzoMassimo==DIM){
					printf("\nNon e' stato trovato alcuno smartphone!\n");
				}else{
					printf("\nPrezzo dello smartphone piu' costoso rispetto a quello minimo --> $%d\n",vet[PrezzoMassimo]);
				}
				break;
			case 7:
				stessoPrezzo(vet,DIM);
				printf("\n");
				break;
			case 8:
				modificaPrezzo(vet,DIM);
				printf("\n");
				break;
			case 9:
				modificaPercentuale(vet,DIM);
				printf("\n");
				break;
			case 0:
				printf("\nGrazie per aver usato il nostro programma!");
				break;
			default:
				printf("\nOpzione non valida, riprova!\n");
				break;
		}
	}
	
}

