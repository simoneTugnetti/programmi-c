#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

// Simone Tugnetti

int main(int argc, char *argv[]){
	char Car;
	
	printf("Inserisci una cifra decimale --> ");
	Car=getch();

	while(Car<'0' || Car>'9'){
		printf("\nValore non valido, riprova --> ");
		Car=getch();
		printf("%c",Car);
	}
	
	printf("\nEcco il valore intero del carattere ASCII --> %d",Car);
	
}
