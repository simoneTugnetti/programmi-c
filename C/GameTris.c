#include <stdio.h>
#include <stdlib.h>
#define R 3
#define C 3
#define COMB 3

// Simone Tugnetti

char leggiCifra();
void inizio(char tris[][C], char ver[][C], int r, int *puntiX, int *puntiO);
void stampa(char tris[][C], int r);
void crea(char tris[][C], int r);
int trovXO(char tris[][C], char ver[][C], int r, char posizione);
int gioco(char tris[][C], int r, char posizione, int *contrXO);
void inserimento(char tris[][C], int r, char posizione, int contrXO);
int controllo(char tris[][C], int r);
char controlloRighe(char tris[][C], int r);
char controlloColonne(char tris[][C], int r);
char controlloDiagonali(char tris[][C], int r);
int vincPerd(char righe, char colonne, char diagonali);

int main(int argc, char* argv[]){
	char tris[R][C],ver[R][C];
	int scelta=1,puntiX=0,puntiO=0;
	crea(ver,R);
	printf("Benvenuto nel gioco del tris!!!");
	while(scelta!=0){
		printf("\n\n1 --> Gioca");
		printf("\n0 --> Esci");
		printf("\nIntroduci la tua scelta --> ");
		scanf("%d",&scelta);
		switch(scelta){
			case 1:
				inizio(tris,ver,R,&puntiX,&puntiO);
				break;
			case 0:
				printf("\nPunti di X --> %d",puntiX);
				printf("\nPunti di O --> %d",puntiO);
				printf("\nGrazie per aver giocato!!");
				break;
			default:
				printf("Scelta non presente, riprova!");
				break;
		}
	}
}

/**
Funzione che d� inizio alla partita
@PARAM matrice del tris, matrice delle verifiche, grandezza delle
righe, punteggio di X e O
@MODIFY punteggio di X o O incrementati, oppure no
*/
void inizio(char tris[][C], char ver[][C], int r, int *puntiX, int *puntiO){
	int contrXO=1,turni=0,risultato=-1;
	char pos;
	crea(tris,R);
	while(turni<9 && risultato==-1){
		if(contrXO==1){
			printf("\nE' il turno di X!");
		}else{
			printf("\nE' il turno di O!");
		}
		printf("\nInserisci la posizione: ");
		pos=leggiCifra();
		while(trovXO(tris,ver,R,pos)==1){
			printf("\nPosizione gia' occupata, riprova: ");
			pos=leggiCifra();
		}
		risultato=gioco(tris,R,pos,&contrXO);
		turni++;
		printf("\n");
		stampa(tris,R);
	}
	if(risultato==1){
		printf("\n\nComplimenti!! \nHa vinto X!!");
		*puntiX=*puntiX+1;
	}else if(risultato==0){
		printf("\n\nComplimenti!! \nHa vinto O!!");
		*puntiO=*puntiO+1;
	}else{
		printf("\n\nComplimenti!! \nNon ha vinto nessuno!!");
	}
}

/**
Funzione che controlla se una determinata posizione vi � gi�
presente X o Y
@PARAM matrice del tris, matrice di verifica, grandezza delle
righe e la posizione da verificare
@RETURN variabile di verifica
*/
int trovXO(char tris[][C], char ver[][C], int r, char posizione){
	int i,j,trov=0;
	for(i=0;i<r && trov==0;i++){
		for(j=0;j<C && trov==0;j++){
			if(ver[i][j]==posizione && (tris[i][j]=='X' || tris[i][j]=='O')){
				trov=1;
			}
		}
	}
	return trov;
}

/**
Funzione che stampa la matrice
@PARAM la matrice del tris e la grandezza delle righe
*/
void stampa(char tris[][C], int r){
	int i,j;
	for(i=0;i<r;i++){
		for(j=0;j<C;j++){
			printf("%c",tris[i][j]);
		}
		printf("\n");
	}
}

/**
Funzione che inserisce un carattere tra 1 e 9 compresi
@RETURN carattere corrispondente
*/
char leggiCifra(){
	char carattere;
	carattere=getch();
	while(carattere<'1' || carattere>'9'){
		carattere=getch();
	}
	printf("%c",carattere);
	return carattere;
}

/**
Funzione che inserisce i dati all'interno della matrice
@PARAM la matrice dei tris e la grandezza delle righe
@MODIFY matrice del tris aggiornata
*/
void crea(char tris[][C], int r){
	int i,j;
	char val='1';
	for(i=0;i<r;i++){
		for(j=0;j<C;j++){
			tris[i][j]=val;
			val++;
		}
	}
}

/**
Funzione che gestisce il gioco del tris
@PARAM la matrice del tris, la grandezza delle righe, la scelta
di dove inserire X e O, la verifica di quale carattere inserito prima
@RETURN vincita o perdita della partita in base alle X e O
*/
int gioco(char tris[][C], int r, char posizione, int *contrXO){
	inserimento(tris,r,posizione,*contrXO);
	if(*contrXO==1){
		*contrXO=*contrXO-1;
	}else{
		*contrXO=*contrXO+1;
	}
	int epilogo=controllo(tris,r);
	return epilogo;
}

/**
Funzione che inserisce X o O in base alle precedenti scelte
@PARAM la matrice tris, la grandezza delle righe, la scelta di
dove inserire X e O, la verifica di quale carattere inserito prima
@MODIFY inserisce X o O nella posizione giusta
*/
void inserimento(char tris[][C],int r, char posizione, int contrXO){
	char valore;
	int i,j,trov=0;
	if(contrXO==1){
		valore='X';
	}else{
		valore='O';
	}
	for(i=0;i<r && trov==0;i++){
		for(j=0;j<C && trov==0;j++){
			if(tris[i][j]==posizione){
				tris[i][j]=valore;
				trov=1;
			}
		}
	}
}

/**
Funzione che controlla se uno dei due giocatori ha vinto
@PARAM matrice del tris e grandezza delle righe
@RETURN variabile di vincita e di perdita
*/
int controllo(char tris[][C], int r){
	char righe=controlloRighe(tris,r);
	char colonne=controlloColonne(tris,r);
	char diagonali=controlloDiagonali(tris,r);
	int vincPerdita=vincPerd(righe,colonne,diagonali);
	return vincPerdita;
}

/**
Funzione che controlla i casi di vincita
@PARAM la variabile del controllo delle righe, colonne e
diagonali
@RETURN 1 se vince X, 0 se vince O, -1 se nessuna
*/
int vincPerd(char righe, char colonne, char diagonali){
	int vinc=-1;
	if(righe=='X'){
		vinc=1;
	}else if(colonne=='X'){
		vinc=1;
	}else if(diagonali=='X'){
		vinc=1;
	}
	if(righe=='O'){
		vinc=0;
	}else if(colonne=='O'){
		vinc=0;
	}else if(diagonali=='O'){
		vinc=0;
	}
	return vinc;
}

/**
Funzione che controlla le righe in cerca di vincite
@PARAM la matrice del tris e la grandezza delle righe
@RETURN X se vince X, O se vince O, N se nessuna
*/
char controlloRighe(char tris[][C], int r){
	int i,j,vincX=0,vincO=0;
	for(i=0;i<r && vincX<COMB;i++){
		vincX=0;
		for(j=0;j<C && vincX<COMB;j++){
			if(tris[i][j]=='X'){
				vincX++;
			}
		}
	}
	if(vincX==COMB){
		return 'X';
	}
	for(i=0;i<r && vincO<COMB;i++){
		vincO=0;
		for(j=0;j<C && vincO<COMB;j++){
			if(tris[i][j]=='O'){
				vincO++;	
			}
		}
	}
	if(vincO==COMB){
		return 'O';
	}
	return 'N';
}

/**
Funzione che controlla le colonne in caso di vincite
@PARAM la matrice del tris e la grandezza delle righe
@RETURN X se vince X, O se vince O, N se nessuna
*/
char controlloColonne(char tris[][C], int r){
	int i,j,vincX=0,vincO=0;
	for(j=0;j<C && vincX<COMB;j++){
		vincX=0;
		for(i=0;i<r && vincX<COMB;i++){
			if(tris[i][j]=='X'){
				vincX++;
			}
		}
	}
	if(vincX==COMB){
		return 'X';
	}
	for(j=0;j<C && vincO<COMB;j++){
		vincO=0;
		for(i=0;i<r && vincO<COMB;i++){
			if(tris[i][j]=='O'){
				vincO++;
			}
		}
	}
	if(vincO==COMB){
		return 'O';
	}
	return 'N';
}

/**
Funzione che controlla le diagonali in caso di vincite
@PARAM la matrice del tris e la grandezza delle righe
@RETURN X se vince X, O se vince O, N se nessuna
*/
char controlloDiagonali(char tris[][C], int r){
	int i,j,vincX,vincO;
	j=0,vincX=0;
	for(i=0;i<r;i++){
		if(tris[i][j]=='X'){
			vincX++;
		}
		j++;
	}
	if(vincX==COMB){
		return 'X';
	}
	j=0,vincX=0;
	for(i=r-1;i>=0;i--){
		if(tris[i][j]=='X'){
			vincX++;
		}
		j++;
	}
	if(vincX==COMB){
		return 'X';
	}
	j=0,vincO=0;
	for(i=0;i<r;i++){
		if(tris[i][j]=='O'){
			vincO++;
		}
		j++;
	}
	if(vincO==COMB){
		return 'O';
	}
	j=0,vincO=0;
	for(i=r-1;i>=0;i--){
		if(tris[i][j]=='O'){
			vincO++;
		}
		j++;
	}
	if(vincO==COMB){
		return 'O';
	}
	return 'N'; 
}

