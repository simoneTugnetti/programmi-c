#include <stdlib.h>
#include <stdio.h>

// Simone Tugnetti

int FunzioneCalcoloDivPari(int Num){
	int Div=1,Somma=0;
	while(Div<=Num){
		if(Num%Div==0 && Div%2==0){
			Somma++;
		}
		Div++;
	}
	return Somma;
}

int main(int argc, char *argv[]){
	int RisultatoDivPari,Num=60;
	RisultatoDivPari=FunzioneCalcoloDivPari(Num);
	printf("%d",RisultatoDivPari); 
}
